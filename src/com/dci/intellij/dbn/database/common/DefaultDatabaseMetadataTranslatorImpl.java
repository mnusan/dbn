package com.dci.intellij.dbn.database.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.data.type.DBDataType;
import com.dci.intellij.dbn.database.DatabaseMetadataTranslatorInterface;
import com.dci.intellij.dbn.language.common.DBLanguage;
import com.dci.intellij.dbn.object.DBConstraint;
import com.dci.intellij.dbn.object.DBSchema;
import com.dci.intellij.dbn.object.DBTrigger.TriggeringEvent;
import com.dci.intellij.dbn.object.common.DBObject;
import com.dci.intellij.dbn.object.common.DBObjectBundle;
import com.dci.intellij.dbn.object.common.DBObjectType;
import com.dci.intellij.dbn.object.lookup.DBObjectRef;
import com.dci.intellij.dbn.object.metadata.DBArgumentMetadata;
import com.dci.intellij.dbn.object.metadata.DBCharsetMetadata;
import com.dci.intellij.dbn.object.metadata.DBClusterMetadata;
import com.dci.intellij.dbn.object.metadata.DBColumnMetadata;
import com.dci.intellij.dbn.object.metadata.DBConstraintColumnMetadata;
import com.dci.intellij.dbn.object.metadata.DBConstraintMetadata;
import com.dci.intellij.dbn.object.metadata.DBDatabaseLinkMetadata;
import com.dci.intellij.dbn.object.metadata.DBDimensionMetadata;
import com.dci.intellij.dbn.object.metadata.DBFunctionMetadata;
import com.dci.intellij.dbn.object.metadata.DBGrantedPrivilegeMetadata;
import com.dci.intellij.dbn.object.metadata.DBGrantedRoleMetadata;
import com.dci.intellij.dbn.object.metadata.DBIndexMetadata;
import com.dci.intellij.dbn.object.metadata.DBMaterializedViewMetadata;
import com.dci.intellij.dbn.object.metadata.DBMethodMetadata;
import com.dci.intellij.dbn.object.metadata.DBNestedTableColumnMetadata;
import com.dci.intellij.dbn.object.metadata.DBNestedTableMetadata;
import com.dci.intellij.dbn.object.metadata.DBPackageMetadata;
import com.dci.intellij.dbn.object.metadata.DBPackageTypeMetadata;
import com.dci.intellij.dbn.object.metadata.DBPrivilegeMetadata;
import com.dci.intellij.dbn.object.metadata.DBProcedureMetadata;
import com.dci.intellij.dbn.object.metadata.DBProgramMetadata;
import com.dci.intellij.dbn.object.metadata.DBRoleMetadata;
import com.dci.intellij.dbn.object.metadata.DBSchemaMetadata;
import com.dci.intellij.dbn.object.metadata.DBSequenceMetadata;
import com.dci.intellij.dbn.object.metadata.DBSynonymMetadata;
import com.dci.intellij.dbn.object.metadata.DBTableMetadata;
import com.dci.intellij.dbn.object.metadata.DBTriggerMetadata;
import com.dci.intellij.dbn.object.metadata.DBTypeAttributeMetadata;
import com.dci.intellij.dbn.object.metadata.DBTypeMetadata;
import com.dci.intellij.dbn.object.metadata.DBUserMetadata;
import com.dci.intellij.dbn.object.metadata.DBViewMetadata;

import static com.dci.intellij.dbn.object.DBTrigger.TRIGGERING_EVENT_ALTER;
import static com.dci.intellij.dbn.object.DBTrigger.TRIGGERING_EVENT_CREATE;
import static com.dci.intellij.dbn.object.DBTrigger.TRIGGERING_EVENT_DDL;
import static com.dci.intellij.dbn.object.DBTrigger.TRIGGERING_EVENT_DELETE;
import static com.dci.intellij.dbn.object.DBTrigger.TRIGGERING_EVENT_DROP;
import static com.dci.intellij.dbn.object.DBTrigger.TRIGGERING_EVENT_INSERT;
import static com.dci.intellij.dbn.object.DBTrigger.TRIGGERING_EVENT_LOGON;
import static com.dci.intellij.dbn.object.DBTrigger.TRIGGERING_EVENT_RENAME;
import static com.dci.intellij.dbn.object.DBTrigger.TRIGGERING_EVENT_TRUNCATE;
import static com.dci.intellij.dbn.object.DBTrigger.TRIGGERING_EVENT_UNKNOWN;
import static com.dci.intellij.dbn.object.DBTrigger.TRIGGERING_EVENT_UPDATE;
import static com.dci.intellij.dbn.object.DBTrigger.TRIGGER_TYPE_AFTER;
import static com.dci.intellij.dbn.object.DBTrigger.TRIGGER_TYPE_BEFORE;
import static com.dci.intellij.dbn.object.DBTrigger.TRIGGER_TYPE_INSTEAD_OF;
import static com.dci.intellij.dbn.object.DBTrigger.TRIGGER_TYPE_UNKNOWN;
import static com.dci.intellij.dbn.object.common.DBObjectType.CONSTRAINT;

/**
 * Default database translate translator.
 * <p>
 * Moved code from DB Entity Implemetations to isolate resulset transformation.
 *
 * @author Manuel Núñez Sánchez (manuel.nsanchez@gmail.com)
 */
public class DefaultDatabaseMetadataTranslatorImpl implements DatabaseMetadataTranslatorInterface {

    //TODO: move rest of createElement implementations if necessary

    @Override
    public String columnDatasetName(ResultSet resultSet) throws SQLException {
        return resultSet.getString("DATASET_NAME");
    }

    @Override
    public String constraintDatasetName(ResultSet resultSet) throws SQLException {
        return resultSet.getString("DATASET_NAME");
    }

    @Override
    public DBConstraintColumnMetadata translate(DBConstraintColumnMetadata target, ResultSet resultSet)
        throws SQLException {
        target.setName(resultSet.getString("DATASET_NAME"));
        target.setColumnName(resultSet.getString("COLUMN_NAME"));
        target.setConstraintName(resultSet.getString("CONSTRAINT_NAME"));
        target.setPosition(resultSet.getInt("POSITION"));

        return target;
    }

    @Override
    public DBArgumentMetadata translate(DBArgumentMetadata target, ResultSet resultSet) throws SQLException {
        target.setOverload(resultSet.getInt("OVERLOAD"));
        target.setPosition(resultSet.getInt("POSITION"));
        target.setSequence(resultSet.getInt("SEQUENCE"));
        String inOut = resultSet.getString("IN_OUT");

        if (inOut != null) {
            target.setInput(inOut.contains("IN"));
            target.setOutput(inOut.contains("OUT"));
        }

        target.setName(resultSet.getString("ARGUMENT_NAME"));

        target.setDataType(DBDataType.get(target.getConnectionHandler(), resultSet));

        return target;
    }

    @Override
    public DBCharsetMetadata translate(DBCharsetMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("CHARSET_NAME"));
        target.setMaxLength(resultSet.getInt("MAX_LENGTH"));
        return target;
    }

    @Override
    public DBClusterMetadata translate(DBClusterMetadata target, ResultSet resultSet) throws SQLException {
        return target.setName(resultSet.getString("CLUSTER_NAME"));
    }

    @Override
    public DBColumnMetadata translate(DBColumnMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("COLUMN_NAME"));
        target.setPosition(resultSet.getInt("POSITION"));

        target.setNullable("Y".equals(resultSet.getString("IS_NULLABLE")));
        target.setPrimaryKey("Y".equals(resultSet.getString("IS_PRIMARY_KEY")));
        target.setForeignKey("Y".equals(resultSet.getString("IS_FOREIGN_KEY")));
        target.setUniqueKey("Y".equals(resultSet.getString("IS_UNIQUE_KEY")));
        target.setHidden("Y".equals(resultSet.getString("IS_HIDDEN")));

        target.setDataType(DBDataType.get(target.getConnectionHandler(), resultSet));

        return target;
    }

    @Override
    public DBConstraintMetadata translate(DBConstraintMetadata target, ResultSet resultSet) throws SQLException {

        target.setName(resultSet.getString("CONSTRAINT_NAME"));

        String checkCondition = resultSet.getString("CHECK_CONDITION");

        String typeString = resultSet.getString("CONSTRAINT_TYPE");
        int constraintType = typeString == null
                                 ? -1
                                 : typeString.equals("CHECK")
                                       ? DBConstraint.CHECK
                                       : typeString.equals("UNIQUE")
                                             ? DBConstraint.UNIQUE_KEY
                                             : typeString.equals("PRIMARY KEY")
                                                   ? DBConstraint.PRIMARY_KEY
                                                   : typeString.equals("FOREIGN KEY")
                                                         ? DBConstraint.FOREIGN_KEY
                                                         : typeString.equals("VIEW CHECK")
                                                               ? DBConstraint.VIEW_CHECK
                                                               : typeString.equals("VIEW READONLY")
                                                                     ? DBConstraint.VIEW_READONLY
                                                                     : -1;

        if (checkCondition == null && constraintType == DBConstraint.CHECK) {
            checkCondition = "";
        }

        target.setCheckCondition(checkCondition);
        target.setConstraintType(constraintType);

        if (constraintType == DBConstraint.FOREIGN_KEY) {
            String fkOwner = resultSet.getString("FK_CONSTRAINT_OWNER");
            String fkName = resultSet.getString("FK_CONSTRAINT_NAME");

            ConnectionHandler connectionHandler = target.getConnectionHandler();
            DBSchema schema = connectionHandler.getObjectBundle().getSchema(fkOwner);
            if (schema != null) {
                DBObjectRef<DBSchema> schemaRef = schema.getRef();
                target.setForeignKeyConstraint(new DBObjectRef<>(schemaRef, CONSTRAINT, fkName));
            }
        }

        target.setEnabled("Y".equals(resultSet.getString("IS_ENABLED")));

        return target;
    }

    @Override
    public DBDatabaseLinkMetadata translate(DBDatabaseLinkMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("DBLINK_NAME"));
        target.setUserName(resultSet.getString("USER_NAME"));
        target.setHost(resultSet.getString("HOST"));
        return target;
    }

    @Override
    public DBDimensionMetadata translate(DBDimensionMetadata target, ResultSet resultSet) throws SQLException {
        return target.setName(resultSet.getString("DIMENSION_NAME"));
    }

    @Override
    public DBFunctionMetadata translate(DBFunctionMetadata target, ResultSet resultSet) throws SQLException {
        return target.setName(resultSet.getString("FUNCTION_NAME"));
    }

    @Override
    public DBGrantedPrivilegeMetadata translate(DBGrantedPrivilegeMetadata target, ResultSet resultSet)
        throws SQLException {
        target.setName(resultSet.getString("GRANTED_PRIVILEGE_NAME"));
        target.setAdmin(resultSet.getString("IS_ADMIN_OPTION").equals("Y"));
        return target;
    }

    @Override
    public DBGrantedRoleMetadata translate(DBGrantedRoleMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("GRANTED_ROLE_NAME"));
        target.setAdmin(resultSet.getString("IS_ADMIN_OPTION").equals("Y"));
        target.setDefaultRole(resultSet.getString("IS_DEFAULT_ROLE").equals("Y"));
        return target;
    }

    @Override
    public DBIndexMetadata translate(DBIndexMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("INDEX_NAME"));
        target.setUnique(resultSet.getString("IS_UNIQUE").equals("Y"));
        target.setValid(resultSet.getString("IS_VALID").equals("Y"));

        return target;
    }

    @Override
    public DBMaterializedViewMetadata translate(DBMaterializedViewMetadata target, ResultSet resultSet)
        throws SQLException {return null;}

    @Override
    public DBMethodMetadata translate(DBMethodMetadata target, ResultSet resultSet) throws SQLException {
        target.setDeterministic(resultSet.getString("IS_DETERMINISTIC").equals("Y"));
        target.setOverload(resultSet.getInt("OVERLOAD"));
        target.setPosition(resultSet.getInt("POSITION"));
        target.setLanguage(DBLanguage.getLanguage(resultSet.getString("LANGUAGE")));

        try {
            target.setValid("Y".equals(resultSet.getString("IS_VALID")));
        }catch (Exception e){
            // status-less resulset
            target.setValid(true);
        }

        try {
            target.setDebug("Y".equals(resultSet.getString("IS_DEBUG")));
        }catch (Exception e){
            // debug-less resulset
            target.setDebug(false);
        }

        return target;
    }

    @Override
    public DBNestedTableMetadata translate(DBNestedTableMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("NESTED_TABLE_NAME"));

        String typeOwner = resultSet.getString("DATA_TYPE_OWNER");
        String typeName = resultSet.getString("DATA_TYPE_NAME");
        DBSchema schema = target.getConnectionHandler().getObjectBundle().getSchema(typeOwner);
        target.setTypeRef(DBObjectRef.from(schema == null ? null : schema.getType(typeName)));
        // todo !!!
        return target;
    }

    @Override
    public DBNestedTableColumnMetadata translate(DBNestedTableColumnMetadata target, ResultSet resultSet)
        throws SQLException {
        // TODO
        return null;
    }

    @Override
    public DBPackageMetadata translate(DBPackageMetadata target, ResultSet resultSet) throws SQLException {
        return target.setName(resultSet.getString("PACKAGE_NAME"));
    }

    @Override
    public DBPackageTypeMetadata translate(DBPackageTypeMetadata target, ResultSet resultSet) throws SQLException {
        return target.setName(resultSet.getString("TYPE_NAME"));
    }

    @Override
    public DBPrivilegeMetadata translate(DBPrivilegeMetadata<? extends DBPrivilegeMetadata> target, ResultSet resultSet)
        throws SQLException {
        return target.setName(resultSet.getString("PRIVILEGE_NAME"));
    }

    @Override
    public DBProcedureMetadata translate(DBProcedureMetadata target, ResultSet resultSet) throws SQLException {
        return target.setName(resultSet.getString("PROCEDURE_NAME"));
    }

    @Override
    public DBProgramMetadata translate(DBProgramMetadata target, ResultSet resultSet) throws SQLException {
        String specValidString = resultSet.getString("IS_SPEC_VALID");
        String bodyValidString = resultSet.getString("IS_BODY_VALID");

        String specDebugString = resultSet.getString("IS_SPEC_DEBUG");
        String bodyDebugString = resultSet.getString("IS_BODY_DEBUG");

        boolean specPresent = specValidString != null;
        boolean specValid = !specPresent || specValidString.equals("Y");
        boolean specDebug = !specPresent || specDebugString.equals("Y");

        boolean bodyPresent = bodyValidString != null;
        boolean bodyValid = !bodyPresent || bodyValidString.equals("Y");
        boolean bodyDebug = !bodyPresent || bodyDebugString.equals("Y");

        target.setSpecPresent(specPresent);
        target.setSpecValid(specValid);
        target.setSpecDebug(specDebug);

        target.setBodyPresent(bodyPresent);
        target.setBodyValid(bodyValid);
        target.setBodyDebug(bodyDebug);

        return target;
    }

    @Override
    public DBRoleMetadata translate(DBRoleMetadata target, ResultSet resultSet) throws SQLException {
        return target.setName(resultSet.getString("ROLE_NAME"));
    }

    @Override
    public DBSchemaMetadata translate(DBSchemaMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("SCHEMA_NAME"));
        target.setPublic(resultSet.getString("IS_PUBLIC").equals("Y"));
        target.setSystem(resultSet.getString("IS_SYSTEM").equals("Y"));
        target.setEmpty(resultSet.getString("IS_EMPTY").equals("Y"));
        return target;
    }

    @Override
    public DBSequenceMetadata translate(DBSequenceMetadata target, ResultSet resultSet) throws SQLException {
        return target.setName(resultSet.getString("SEQUENCE_NAME"));
    }

    @Override
    public DBSynonymMetadata translate(DBSynonymMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("SYNONYM_NAME"));
        String schemaName = resultSet.getString("OBJECT_OWNER");
        String objectName = resultSet.getString("OBJECT_NAME");
        DBObjectType objectType = DBObjectType.get(resultSet.getString("OBJECT_TYPE"), DBObjectType.ANY);

        ConnectionHandler connectionHandler = target.getConnectionHandler();
        DBSchema schema = connectionHandler.getObjectBundle().getSchema(schemaName);
        if (schema != null) {
            DBObjectRef schemaRef = schema.getRef();
            target.setUnderlyingObject(new DBObjectRef<DBObject>(schemaRef, objectType, objectName));
        }

        target.setValid(resultSet.getString("IS_VALID").equals("Y"));

        return target;
    }

    @Override
    public DBTableMetadata translate(DBTableMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("TABLE_NAME"));
        target.setTemporary(resultSet.getString("IS_TEMPORARY").equals("Y"));
        return target;
    }

    @Override
    public DBTriggerMetadata translate(DBTriggerMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("TRIGGER_NAME"));
        target.setForEachRow(resultSet.getString("IS_FOR_EACH_ROW").equals("Y"));

        String triggerTypeString = resultSet.getString("TRIGGER_TYPE");
        target.setTriggerType(triggerTypeString.contains("BEFORE")
                                  ? TRIGGER_TYPE_BEFORE
                                  : triggerTypeString.contains("AFTER")
                                        ? TRIGGER_TYPE_AFTER
                                        : triggerTypeString.contains("INSTEAD OF")
                                              ? TRIGGER_TYPE_INSTEAD_OF
                                              : TRIGGER_TYPE_UNKNOWN);

        String triggeringEventString = resultSet.getString("TRIGGERING_EVENT");

        List<TriggeringEvent> triggeringEventList = new ArrayList<TriggeringEvent>();
        if (triggeringEventString.contains("INSERT")) {
            triggeringEventList.add(TRIGGERING_EVENT_INSERT);
        }
        if (triggeringEventString.contains("UPDATE")) {
            triggeringEventList.add(TRIGGERING_EVENT_UPDATE);
        }
        if (triggeringEventString.contains("DELETE")) {
            triggeringEventList.add(TRIGGERING_EVENT_DELETE);
        }
        if (triggeringEventString.contains("TRUNCATE")) {
            triggeringEventList.add(TRIGGERING_EVENT_TRUNCATE);
        }
        if (triggeringEventString.contains("CREATE")) {
            triggeringEventList.add(TRIGGERING_EVENT_CREATE);
        }
        if (triggeringEventString.contains("ALTER")) {
            triggeringEventList.add(TRIGGERING_EVENT_ALTER);
        }
        if (triggeringEventString.contains("DROP")) {
            triggeringEventList.add(TRIGGERING_EVENT_DROP);
        }
        if (triggeringEventString.contains("RENAME")) {
            triggeringEventList.add(TRIGGERING_EVENT_RENAME);
        }
        if (triggeringEventString.contains("LOGON")) {
            triggeringEventList.add(TRIGGERING_EVENT_LOGON);
        }
        if (triggeringEventString.contains("DDL")) {
            triggeringEventList.add(TRIGGERING_EVENT_DDL);
        }
        if (triggeringEventList.isEmpty()) {
            triggeringEventList.add(TRIGGERING_EVENT_UNKNOWN);
        }

        target.setTriggeringEvents(triggeringEventList.toArray(new TriggeringEvent[0]));

        target.setEnabled(resultSet.getString("IS_ENABLED").equals("Y"));
        target.setValid(resultSet.getString("IS_VALID").equals("Y"));
        target.setDebug(resultSet.getString("IS_DEBUG").equals("Y"));

        return target;
    }

    @Override
    public DBTypeMetadata translate(DBTypeMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("TYPE_NAME"));
        target.setSuperTypeOwner(resultSet.getString("SUPERTYPE_OWNER"));
        target.setSuperTypeName(resultSet.getString("SUPERTYPE_NAME"));

        String typecode = resultSet.getString("TYPECODE");
        target.setCollection("COLLECTION".equals(typecode));
        ConnectionHandler connectionHandler = target.getConnectionHandler();
        target.setNativeDataType(connectionHandler.getObjectBundle().getNativeDataType(typecode));
        if (target.isCollection()) {
            target.setCollectionElementTypeRef(new DBDataType.Ref(resultSet, "COLLECTION_"));
        }
        return target;
    }

    @Override
    public DBTypeAttributeMetadata translate(DBTypeAttributeMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("ATTRIBUTE_NAME"));
        target.setPosition(resultSet.getInt("POSITION"));
        target.setDataType(DBDataType.get(target.getConnectionHandler(), resultSet));
        return target;
    }

    @Override
    public DBUserMetadata translate(DBUserMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("USER_NAME"));
        target.setExpired(resultSet.getString("IS_EXPIRED").equals("Y"));
        target.setLocked(resultSet.getString("IS_LOCKED").equals("Y"));

        return target;
    }

    @Override
    public DBViewMetadata translate(DBViewMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("VIEW_NAME"));
        target.setSystemView(resultSet.getString("IS_SYSTEM_VIEW").equals("Y"));
        String typeOwner = resultSet.getString("VIEW_TYPE_OWNER");
        String typeName = resultSet.getString("VIEW_TYPE");
        if (typeOwner != null && typeName != null) {
            DBObjectBundle objectBundle = target.getConnectionHandler().getObjectBundle();
            DBSchema typeSchema = objectBundle.getSchema(typeOwner);
            target.setType(typeSchema == null ? null : typeSchema.getType(typeName));
        }
        return target;
    }

}
