package com.dci.intellij.dbn.database.generic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.dci.intellij.dbn.common.LoggerFactory;
import com.dci.intellij.dbn.common.util.CommonUtil;
import com.dci.intellij.dbn.connection.ResourceUtil;
import com.dci.intellij.dbn.connection.jdbc.DBNConnection;
import com.dci.intellij.dbn.connection.jdbc.DBNResultSetUnion;
import com.dci.intellij.dbn.database.DatabaseInterfaceProvider;
import com.dci.intellij.dbn.database.common.DatabaseMetadataInterfaceImpl;
import com.intellij.openapi.diagnostic.Logger;

/**
 * Metadata Loader implementation based on standard JDBC Api
 * <p>
 * TODO: review log: level, messages
 */
public class GenericMetadataInterface extends DatabaseMetadataInterfaceImpl {

    private static final Logger LOGGER = LoggerFactory.createLogger();

    public GenericMetadataInterface(DatabaseInterfaceProvider provider) {
        super("generic_metadata_interface.xml", provider);
    }

    @Override
    public ResultSet loadCompileObjectErrors(String ownerName, String objectName, DBNConnection connection)
        throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadSchemas(DBNConnection connection) throws SQLException {
        try {
            return new DBNResultSetUnion(connection.getMetaData().getSchemas());
        } catch (Exception e) {
            LOGGER.warn("Unable to load schemas for " + connection.getName(), e);
            return null;
        }
    }

    @Override
    public ResultSet loadTables(String ownerName, DBNConnection connection) throws SQLException {
        try {
            return new DBNResultSetUnion(
                connection.getMetaData().getTables(null, ownerName, null, new String[] {"TABLE"}));
        } catch (Exception e) {
            LOGGER.warn("Unable to load tables for " + connection.getName(), e);
            return null;
        }
    }

    @Override
    public ResultSet loadViews(String ownerName, DBNConnection connection) throws SQLException {
        try {
            return new DBNResultSetUnion(
                connection.getMetaData().getTables(null, ownerName, null, new String[] {"VIEW"}));
        } catch (Exception e) {
            LOGGER.warn("Unable to load views for " + connection.getName(), e);
            return null;
        }
    }

    @Override
    public ResultSet loadIndexes(String ownerName, String tableName, DBNConnection connection) throws SQLException {
        try {
            return new DBNResultSetUnion(
                connection.getMetaData().getIndexInfo(null, ownerName, tableName, false, true));
        } catch (Exception e) {
            LOGGER.warn("Unable to load indexes for " + connection.getName(), e);
            return null;
        }
    }

    @Override
    public ResultSet loadAllIndexes(String ownerName, DBNConnection connection) throws SQLException {

        ResultSet tables = new DBNResultSetUnion(
            connection.getMetaData().getTables(null, ownerName, null, new String[] {"TABLE"}));

        List<ResultSet> resultSets = new ArrayList<>();

        while (tables != null && tables.next()) {
            try {
                resultSets.add(connection.getMetaData()
                                   .getIndexInfo(null, ownerName, tables.getString("TABLE_NAME"), false, true));
            } catch (Exception e) {
                LOGGER.warn("Unable to load all indexes for " + connection.getName(), e);
            }
        }

        ResourceUtil.close(tables);

        return new DBNResultSetUnion(resultSets.toArray(new ResultSet[resultSets.size()]));
    }

    @Override
    public ResultSet loadColumns(String ownerName, String datasetName, DBNConnection connection) throws SQLException {

        List<ResultSet> resultSets = new ArrayList<>();

        // BE CAREFULL: this mix 2 different resultsets!!
        try {
            resultSets.add(connection.getMetaData().getColumns(null, ownerName, datasetName, null));
        } catch (Exception e) {
            LOGGER.warn("Unable to load columns for " + connection.getName(), e);
        }
        try {
            resultSets.add(connection.getMetaData().getPseudoColumns(null, ownerName, datasetName, null));
        } catch (Exception e) {
            LOGGER.warn("Unable to load columns for " + connection.getName(), e);
        }
        return new DBNResultSetUnion(resultSets.toArray(new ResultSet[resultSets.size()]));
    }

    @Override
    public ResultSet loadAllColumns(String ownerName, DBNConnection connection) throws SQLException {
        List<ResultSet> resultSets = new ArrayList<>();

        // BE CAREFULL: this mix 2 different resultsets!!
        try {
            resultSets.add(connection.getMetaData().getColumns(null, ownerName, null, null));
        } catch (Exception e) {
            LOGGER.warn("Unable to load all columns for " + connection.getName(), e);
        }
        try {
            resultSets.add(connection.getMetaData().getPseudoColumns(null, ownerName, null, null));
        } catch (Exception e) {
            LOGGER.warn("Unable to load all columns for " + connection.getName(), e);
        }

        return new DBNResultSetUnion(resultSets.toArray(new ResultSet[resultSets.size()]));
    }

    @Override
    public ResultSet loadIndexRelations(String ownerName, String tableName, DBNConnection connection)
        throws SQLException {
        try {
            return new DBNResultSetUnion(
                connection.getMetaData().getIndexInfo(null, ownerName, tableName, false, true));
        } catch (Exception e) {
            LOGGER.warn("Unable to load index relations for " + connection.getName(), e);
            return null;
        }
    }

    @Override
    public ResultSet loadAllIndexRelations(String ownerName, DBNConnection connection) throws SQLException {
        return loadAllIndexes(ownerName, connection);
    }

    @Override
    public ResultSet loadConstraintRelations(String ownerName, String datasetName, DBNConnection connection)
        throws SQLException {

        ResultSet tables = new DBNResultSetUnion(
            connection.getMetaData().getTables(null, ownerName, datasetName, new String[] {"TABLE"}));

        List<ResultSet> resultSets = new ArrayList<>();
        // BE CAREFULL: this mix 2 different resultsets!!
        // primary keys (pk, unique) and imported keys (foreign keys)
        while (tables != null && tables.next()) {
            try {
                resultSets
                    .add(connection.getMetaData().getPrimaryKeys(null, ownerName, tables.getString("TABLE_NAME")));
            } catch (Exception e) {
                LOGGER.warn("Unable to load primary key constrait relations for " + connection.getName(), e);
            }
            try {
                resultSets
                    .add(connection.getMetaData().getImportedKeys(null, ownerName, tables.getString("TABLE_NAME")));
            } catch (Exception e) {
                LOGGER.warn("Unable to load foreign key constrait relations for " + connection.getName(), e);
            }
        }

        ResourceUtil.close(tables);

        return new DBNResultSetUnion(resultSets.toArray(new ResultSet[resultSets.size()]));
    }

    @Override
    public ResultSet loadAllConstraintRelations(String ownerName, DBNConnection connection) throws SQLException {

        ResultSet tables = new DBNResultSetUnion(
            connection.getMetaData().getTables(null, ownerName, null, new String[] {"TABLE"}));

        List<ResultSet> resultSets = new ArrayList<>();
        // BE CAREFULL: this mix 2 different resultsets!!
        // primary keys (pk, unique) and imported keys (foreign keys)
        while (tables != null && tables.next()) {
            try {
                resultSets
                    .add(connection.getMetaData().getPrimaryKeys(null, ownerName, tables.getString("TABLE_NAME")));
            } catch (Exception e) {
                LOGGER.warn("Unable to load all primary key constrait relations for " + connection.getName(), e);
            }
            try {
                resultSets
                    .add(connection.getMetaData().getImportedKeys(null, ownerName, tables.getString("TABLE_NAME")));
            } catch (Exception e) {
                LOGGER.warn("Unable to load all foreign key constrait relations for " + connection.getName(), e);
            }
        }

        ResourceUtil.close(tables);

        return new DBNResultSetUnion(resultSets.toArray(new ResultSet[resultSets.size()]));
    }

    @Override
    public ResultSet loadConstraints(String ownerName, String datasetName, DBNConnection connection)
        throws SQLException {

        ResultSet tables = new DBNResultSetUnion(
            connection.getMetaData().getTables(null, ownerName, datasetName, new String[] {"TABLE"}));

        List<ResultSet> resultSets = new ArrayList<>();

        // BE CAREFULL: this mix 2 different resultsets!!
        // primary keys (pk, unique) and imported keys (foreign keys)
        while (tables != null && tables.next()) {
            try {
                resultSets
                    .add(connection.getMetaData().getPrimaryKeys(null, ownerName, tables.getString("TABLE_NAME")));
            } catch (Exception e) {
                LOGGER.warn("Unable to load primary keys constrait for " + connection.getName(), e);
            }
            try {
                resultSets
                    .add(connection.getMetaData().getImportedKeys(null, ownerName, tables.getString("TABLE_NAME")));
            } catch (Exception e) {
                LOGGER.warn("Unable to load foreign keys constrait for " + connection.getName(), e);
            }
        }

        ResourceUtil.close(tables);

        return new DBNResultSetUnion(resultSets.toArray(new ResultSet[resultSets.size()]));
    }

    @Override
    public ResultSet loadAllConstraints(String ownerName, DBNConnection connection) throws SQLException {

        ResultSet tables = new DBNResultSetUnion(
            connection.getMetaData().getTables(null, ownerName, null, new String[] {"TABLE"}));

        List<ResultSet> resultSets = new ArrayList<>();

        // BE CAREFULL: this mix 2 different resultsets!!
        // primary keys (pk, unique) and imported keys (foreign keys)
        while (tables != null && tables.next()) {
            try {
                resultSets
                    .add(connection.getMetaData().getPrimaryKeys(null, ownerName, tables.getString("TABLE_NAME")));
            } catch (Exception e) {
                LOGGER.warn("Unable to load all primary keys constrait for " + connection.getName(), e);
            }
            try {
                resultSets
                    .add(connection.getMetaData().getImportedKeys(null, ownerName, tables.getString("TABLE_NAME")));
            } catch (Exception e) {
                LOGGER.warn("Unable to load all foreign keys constrait for " + connection.getName(), e);
            }
        }

        ResourceUtil.close(tables);

        return new DBNResultSetUnion(resultSets.toArray(new ResultSet[resultSets.size()]));
    }

    @Override
    public ResultSet loadFunctions(String ownerName, DBNConnection connection) throws SQLException {
        try {
            return new DBNResultSetUnion(connection.getMetaData().getFunctions(null, ownerName, null));
        } catch (Exception e) {
            LOGGER.warn("Unable to load functions for " + connection.getName(), e);
            return null;
        }
    }

    @Override
    public ResultSet loadProcedures(String ownerName, DBNConnection connection) throws SQLException {
        try {
            return new DBNResultSetUnion(connection.getMetaData().getProcedures(null, ownerName, null));
        } catch (Exception e) {
            LOGGER.warn("Unable to load procedures for " + connection.getName(), e);
            return null;
        }
    }

    @Override
    public ResultSet loadMethodArguments(String ownerName,
        String methodName,
        String methodType,
        int overload,
        DBNConnection connection) throws SQLException {

        List<ResultSet> resultSets = new ArrayList<>();

        // BE CAREFULL: this mix 2 different resultsets!!
        try {
            resultSets.add(connection.getMetaData().getFunctionColumns(null, ownerName, methodName, null));
        } catch (Exception e) {
            LOGGER.warn("Unable to load functions arguments for " + connection.getName(), e);
        }
        try {
            resultSets.add(connection.getMetaData().getProcedureColumns(null, ownerName, methodName, null));
        } catch (Exception e) {
            LOGGER.warn("Unable to load procedures arguments for " + connection.getName(), e);
        }
        return new DBNResultSetUnion(resultSets.toArray(new ResultSet[resultSets.size()]));
    }

    @Override
    public ResultSet loadAllMethodArguments(String ownerName, DBNConnection connection) throws SQLException {
        List<ResultSet> resultSets = new ArrayList<>();

        // BE CAREFULL: this mix 2 different resultsets!!
        try {
            resultSets.add(connection.getMetaData().getFunctionColumns(null, ownerName, null, null));
        } catch (Exception e) {
            LOGGER.warn("Unable to load all functions arguments for " + connection.getName(), e);
        }
        try {
            resultSets.add(connection.getMetaData().getProcedureColumns(null, ownerName, null, null));
        } catch (Exception e) {
            LOGGER.warn("Unable to load all procedures arguments for " + connection.getName(), e);
        }
        return new DBNResultSetUnion(resultSets.toArray(new ResultSet[resultSets.size()]));
    }

    /*********************************************************
     *                        TYPES                          *
     *********************************************************/
    @Override
    public ResultSet loadTypes(String ownerName, DBNConnection connection) throws SQLException {
        List<ResultSet> resultSets = new ArrayList<>();

        // BE CAREFULL: this mix 2 different resultsets!!
        try {
            resultSets.add(connection.getMetaData().getTypeInfo());
        } catch (Exception e) {
            LOGGER.warn("Unable to load types for " + connection.getName(), e);
        }
        try {
            resultSets.add(connection.getMetaData().getUDTs(null, ownerName, null, null));
        } catch (Exception e) {
            LOGGER.warn("Unable to load UDT types for " + connection.getName(), e);
        }
        return new DBNResultSetUnion(resultSets.toArray(new ResultSet[resultSets.size()]));
    }

    // NOT IMPLEMENTED YET - not required for basic working

    @Override
    public ResultSet loadTypeAttributes(String ownerName, String typeName, DBNConnection connection)
        throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadAllTypeAttributes(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadProgramTypeAttributes(String ownerName,
        String programName,
        String typeName,
        DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadTypeFunctions(String ownerName, String typeName, DBNConnection connection)
        throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadAllTypeFunctions(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadTypeProcedures(String ownerName, String typeName, DBNConnection connection)
        throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadAllTypeProcedures(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadProgramMethodArguments(String ownerName,
        String programName,
        String methodName,
        int overload,
        DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadUsers(DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadCharsets(DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadRoles(DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadAllUserRoles(DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadSystemPrivileges(DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadObjectPrivileges(DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadAllUserPrivileges(DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadAllRolePrivileges(DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadAllRoleRoles(DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadClusters(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadMaterializedViews(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadNestedTables(String ownerName, String tableName, DBNConnection connection)
        throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadAllNestedTables(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadDatabaseTriggers(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadDatasetTriggers(String ownerName, String datasetName, DBNConnection connection)
        throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadAllDatasetTriggers(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadDimensions(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadPackages(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadPackageFunctions(String ownerName, String packageName, DBNConnection connection)
        throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadAllPackageFunctions(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadPackageProcedures(String ownerName, String packageName, DBNConnection connection)
        throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadAllPackageProcedures(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadPackageTypes(String ownerName, String packageName, DBNConnection connection)
        throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadAllPackageTypes(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadDatabaseLinks(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadSequences(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadSynonyms(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadReferencedObjects(String ownerName, String objectName, DBNConnection connection)
        throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadReferencingObjects(String ownerName, String objectName, DBNConnection connection)
        throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadReferencingSchemas(String ownerName, String objectName, DBNConnection connection)
        throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadViewSourceCode(String ownerName, String viewName, DBNConnection connection)
        throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadMaterializedViewSourceCode(String ownerName, String viewName, DBNConnection connection)
        throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadDatabaseTriggerSourceCode(String ownerName, String triggerName, DBNConnection connection)
        throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadDatasetTriggerSourceCode(String tableOwner,
        String tableName,
        String ownerName,
        String triggerName,
        DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadObjectSourceCode(String ownerName,
        String objectName,
        String objectType,
        DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadObjectSourceCode(String ownerName,
        String objectName,
        String objectType,
        int overload,
        DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadObjectChangeTimestamp(String ownerName,
        String objectName,
        String objectType,
        DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadInvalidObjects(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadDebugObjects(String ownerName, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadSessions(DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadSessionCurrentSql(Object sessionId, DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public ResultSet loadExplainPlan(DBNConnection connection) throws SQLException {
        return null;
    }

    @Override
    public String createDateString(Date date) {
        String dateString = META_DATE_FORMAT.get().format(date);
        return "str_to_date('" + dateString + "', '%Y-%m-%d %T')";
    }

    @Override
    public boolean isValid(DBNConnection connection) {
        return connection.isValid();
    }

}
