package com.dci.intellij.dbn.database.generic;

import java.sql.JDBCType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dci.intellij.dbn.common.LoggerFactory;
import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.connection.jdbc.DBNResultSetUnion;
import com.dci.intellij.dbn.data.type.DBDataType;
import com.dci.intellij.dbn.database.common.DefaultDatabaseMetadataTranslatorImpl;
import com.dci.intellij.dbn.object.DBConstraint;
import com.dci.intellij.dbn.object.DBSchema;
import com.dci.intellij.dbn.object.lookup.DBObjectRef;
import com.dci.intellij.dbn.object.metadata.DBArgumentMetadata;
import com.dci.intellij.dbn.object.metadata.DBCharsetMetadata;
import com.dci.intellij.dbn.object.metadata.DBClusterMetadata;
import com.dci.intellij.dbn.object.metadata.DBColumnMetadata;
import com.dci.intellij.dbn.object.metadata.DBConstraintColumnMetadata;
import com.dci.intellij.dbn.object.metadata.DBConstraintMetadata;
import com.dci.intellij.dbn.object.metadata.DBDatabaseLinkMetadata;
import com.dci.intellij.dbn.object.metadata.DBDimensionMetadata;
import com.dci.intellij.dbn.object.metadata.DBFunctionMetadata;
import com.dci.intellij.dbn.object.metadata.DBGrantedPrivilegeMetadata;
import com.dci.intellij.dbn.object.metadata.DBGrantedRoleMetadata;
import com.dci.intellij.dbn.object.metadata.DBIndexMetadata;
import com.dci.intellij.dbn.object.metadata.DBMaterializedViewMetadata;
import com.dci.intellij.dbn.object.metadata.DBMethodMetadata;
import com.dci.intellij.dbn.object.metadata.DBNestedTableColumnMetadata;
import com.dci.intellij.dbn.object.metadata.DBNestedTableMetadata;
import com.dci.intellij.dbn.object.metadata.DBPackageMetadata;
import com.dci.intellij.dbn.object.metadata.DBPackageTypeMetadata;
import com.dci.intellij.dbn.object.metadata.DBPrivilegeMetadata;
import com.dci.intellij.dbn.object.metadata.DBProcedureMetadata;
import com.dci.intellij.dbn.object.metadata.DBProgramMetadata;
import com.dci.intellij.dbn.object.metadata.DBRoleMetadata;
import com.dci.intellij.dbn.object.metadata.DBSchemaMetadata;
import com.dci.intellij.dbn.object.metadata.DBSequenceMetadata;
import com.dci.intellij.dbn.object.metadata.DBSynonymMetadata;
import com.dci.intellij.dbn.object.metadata.DBTableMetadata;
import com.dci.intellij.dbn.object.metadata.DBTriggerMetadata;
import com.dci.intellij.dbn.object.metadata.DBTypeAttributeMetadata;
import com.dci.intellij.dbn.object.metadata.DBTypeMetadata;
import com.dci.intellij.dbn.object.metadata.DBUserMetadata;
import com.dci.intellij.dbn.object.metadata.DBViewMetadata;
import com.intellij.openapi.diagnostic.Logger;

import static java.sql.DatabaseMetaData.functionColumnIn;
import static java.sql.DatabaseMetaData.functionColumnInOut;
import static java.sql.DatabaseMetaData.functionColumnOut;

import static com.dci.intellij.dbn.object.common.DBObjectType.CONSTRAINT;

/**
 * Standard JDBC Database metadata translator.
 * <p>
 * It's slower than direct query to information schema, but ensures full compatibility with any JDBC driver
 *
 * @author Manuel Núñez Sánchez (manuel.nsanchez@gmail.com)
 */
public class GenericMetadataTranslatorImpl extends DefaultDatabaseMetadataTranslatorImpl {

    private static final Logger LOGGER = LoggerFactory.createLogger();

    @Override
    public String columnDatasetName(ResultSet resultSet) throws SQLException {
        return resultSet.getString("TABLE_NAME");
    }

    /**
     * @param resultset
     *     please not that we receive a {@link DBNResultSetUnion} composed by primaryKeys and importedKeys (foreign
     *     keys)
     * @return
     * @throws SQLException
     */
    @Override
    public String constraintDatasetName(ResultSet resultSet) throws SQLException {

        String datasetName = null;

        try {
            // if resultset is primaryKeys
            datasetName = resultSet.getString("TABLE_NAME");
        } catch (Exception e) {
            // if resultset is importedKeys
            datasetName = resultSet.getString("FKTABLE_NAME");
        }

        return datasetName;
    }

    /**
     * @param target
     * @param resultset
     *     please not that we receive a {@link DBNResultSetUnion} composed by primaryKeys and importedKeys (foreign
     *     keys)
     * @return
     * @throws SQLException
     */
    @Override
    public DBConstraintColumnMetadata translate(DBConstraintColumnMetadata target, ResultSet resultSet)
        throws SQLException {

        String datasetName = null;
        String columnName = null;
        String constraintName = null;
        try {
            // if resultset is primaryKeys
            datasetName = resultSet.getString("TABLE_NAME");
            columnName = resultSet.getString("COLUMN_NAME");
            constraintName = resultSet.getString("PK_NAME") != null
                                 ? "pk_" + datasetName + columnName
                                 : "unq_" + datasetName + columnName;
        } catch (Exception e) {
            // if resultset is importedKeys
            datasetName = resultSet.getString("FKTABLE_NAME");
            columnName = resultSet.getString("FKCOLUMN_NAME");
            constraintName = resultSet.getString("FK_NAME");
        }

        target.setName(datasetName);
        target.setColumnName(columnName);
        target.setConstraintName(constraintName);
        target.setPosition(resultSet.getInt("KEY_SEQ"));

        return target;
    }

    @Override
    public DBColumnMetadata translate(DBColumnMetadata target, ResultSet resultSet) throws SQLException {
        String name = resultSet.getString("COLUMN_NAME");
        target.setName(name);
        try {
            target.setNullable("YES".equals(resultSet.getString("IS_NULLABLE")));
        } catch (Exception e) {
            target.setNullable(true);
        }

        try {
            target.setPosition(resultSet.getInt("ORDINAL_POSITION"));
        } catch (Exception e) {
            // comes from speudo columns
            target.setPosition(0);
        }

        List<String> pkColumns = new ArrayList<>();
        List<String> uniqueColumns = new ArrayList<>();
        List<String> fkColumns = new ArrayList<>();
        List<String> pseudoColumns = new ArrayList<>();
        try (ResultSet primaryKeys = new DBNResultSetUnion(
            target.getConnectionHandler().getMainConnection().getMetaData()
                .getPrimaryKeys(null, resultSet.getString("TABLE_SCHEM"), resultSet.getString("TABLE_NAME")));
            ResultSet importedKeys = new DBNResultSetUnion(
                target.getConnectionHandler().getMainConnection().getMetaData()
                    .getImportedKeys(null, resultSet.getString("TABLE_SCHEM"), resultSet.getString("TABLE_NAME")));
            ResultSet pseudoCol = new DBNResultSetUnion(target.getConnectionHandler().getMainConnection().getMetaData()
                                                            .getPseudoColumns(null, resultSet.getString("TABLE_SCHEM"),
                                                                resultSet.getString("TABLE_NAME"), name))) {
            // primary/unique columns
            while (primaryKeys != null && primaryKeys.next()) {
                if (primaryKeys.getString("PK_NAME") != null) {
                    pkColumns.add(primaryKeys.getString("COLUMN_NAME"));
                } else {
                    uniqueColumns.add(primaryKeys.getString("COLUMN_NAME"));
                }
            }
            // foreign columns
            while (importedKeys != null && importedKeys.next()) {
                fkColumns.add(importedKeys.getString("FKCOLUMN_NAME"));
            }
            // pseudo/hidden cols
            while (pseudoCol != null && pseudoCol.next()) {
                pseudoColumns.add(pseudoCol.getString("COLUMN_NAME"));
            }
        }

        target.setPrimaryKey(pkColumns.contains(name));
        target.setUniqueKey(uniqueColumns.contains(name));
        target.setForeignKey(fkColumns.contains(name));
        target.setHidden(pseudoColumns.contains(name));

        Integer length = null;
        Integer precision = null;
        Integer scale = null;

        try {
            length = resultSet.getInt("COLUMN_SIZE");
            precision = resultSet.getInt("COLUMN_SIZE");
            scale = resultSet.getInt("DECIMAL_DIGITS");
        } catch (Exception e) {
            // ignore column info
        }

        boolean set = false;
        String typeName = null;

        try {
            typeName = resultSet.getString("TYPE_NAME");
            if (typeName == null || typeName.isEmpty()) {
                typeName = JDBCType.valueOf(resultSet.getInt("DATA_TYPE")).getName();
            }
        } catch (Exception e) {
            typeName = JDBCType.OTHER.getName();
        }

        target.setDataType(DBDataType.get(target.getConnectionHandler(), typeName, length == null ? 0 : length,
            precision == null ? 0 : precision, scale == null ? 0 : scale, set));

        return target;
    }

    @Override
    public DBIndexMetadata translate(DBIndexMetadata target, ResultSet resultSet) throws SQLException {
        String name = null;
        try {
            name = resultSet.getString("INDEX_NAME");
            target.setUnique(!resultSet.getBoolean("NON_UNIQUE"));
        } catch (Exception e) {
            target.setUnique(false);
        }
        target.setValid(true);
        name = name == null ? resultSet.getString("TABLE_NAME") + "_INDEX_STATISTIC" : name;
        target.setName(name);
        return target;
    }

    @Override
    public DBSchemaMetadata translate(DBSchemaMetadata target, ResultSet resultSet) throws SQLException {
        String name = resultSet.getString("TABLE_SCHEM");

        target.setName(name);

        boolean isEmpty = false;

        // TODO: add rest of metada artifacts if necessary to check emptiness
        try {
            try (ResultSet tables = new DBNResultSetUnion(
                target.getConnectionHandler().getMainConnection().getMetaData().getTables(null, name, null, null));
                ResultSet functions = new DBNResultSetUnion(
                    target.getConnectionHandler().getMainConnection().getMetaData().getFunctions(null, name, null));
                ResultSet procedures = new DBNResultSetUnion(
                    target.getConnectionHandler().getMainConnection().getMetaData().getProcedures(null, name, null));) {
                isEmpty = !tables.next() && !functions.next() && !procedures.next();
            }
        } catch (Exception e) {

        }
        target.setPublic(false);
        target.setSystem(false);
        target.setEmpty(isEmpty);

        return target;
    }

    @Override
    public DBTableMetadata translate(DBTableMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("TABLE_NAME"));
        String typeName = resultSet.getString("TABLE_TYPE");
        target.setTemporary(typeName.contains("TEMPORARY"));
        return target;
    }

    @Override
    public DBConstraintMetadata translate(DBConstraintMetadata target, ResultSet resultSet) throws SQLException {

        String datasetName = null;
        String columnName = null;
        String constraintName = null;
        int constraintType = -1;
        try {
            // if resultset comes from primaryKeys
            datasetName = resultSet.getString("TABLE_NAME");
            columnName = resultSet.getString("COLUMN_NAME");
            String pkName = resultSet.getString("PK_NAME");
            constraintName = pkName != null ? "pk_" + datasetName + columnName : "unq_" + datasetName + columnName;
            constraintType = pkName != null ? DBConstraint.PRIMARY_KEY : DBConstraint.UNIQUE_KEY;
        } catch (Exception e) {
            // if resultset comes from importedKeys
            datasetName = resultSet.getString("FKTABLE_NAME");
            columnName = resultSet.getString("FKCOLUMN_NAME");
            constraintName = resultSet.getString("FK_NAME");
            constraintName = constraintName == null ? "FK_" + datasetName + "_" + columnName : constraintName;
            constraintType = DBConstraint.FOREIGN_KEY;
        }

        String checkCondition = "";

        target.setCheckCondition(checkCondition);
        target.setConstraintType(constraintType);

        if (constraintType == DBConstraint.FOREIGN_KEY) {
            try {
                String fkOwner = resultSet.getString("FKTABLE_SCHEM");
                String fkName = resultSet.getString("FK_NAME");

                ConnectionHandler connectionHandler = target.getConnectionHandler();
                DBSchema schema = connectionHandler.getObjectBundle().getSchema(fkOwner);
                if (schema != null) {
                    DBObjectRef<DBSchema> schemaRef = schema.getRef();
                    target.setForeignKeyConstraint(new DBObjectRef<>(schemaRef, CONSTRAINT, fkName));
                }
            } catch (Exception e) {
                // ignore it
            }
        }

        target.setName(constraintName);

        target.setEnabled(true);

        return target;
    }

    @Override
    public DBArgumentMetadata translate(DBArgumentMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("COLUMN_NAME"));
        target.setOverload(0);
        target.setPosition(resultSet.getInt("ORDINAL_POSITION"));
        target.setSequence(0);

        int columnType = resultSet.getInt("COLUMN_TYPE");

        target.setInput(columnType == functionColumnInOut || columnType == functionColumnIn);
        target.setOutput(columnType == functionColumnInOut || columnType == functionColumnOut);

        boolean set = false;
        Integer length = null;
        Integer precision = null;
        Integer scale = null;

        try {
            length = resultSet.getInt("LENGTH");
            precision = resultSet.getInt("PRECISION");
            scale = resultSet.getInt("SCALE");
        } catch (Exception e) {
            // ignore column info
        }
        String typeName = null;

        try {
            typeName = resultSet.getString("TYPE_NAME");
            if (typeName == null || typeName.isEmpty()) {
                typeName = JDBCType.valueOf(resultSet.getInt("DATA_TYPE")).getName();
            }
        } catch (Exception e) {
            typeName = JDBCType.OTHER.getName();
        }

        target.setDataType(DBDataType.get(target.getConnectionHandler(), typeName, length == null ? 0 : length,
            precision == null ? 0 : precision, scale == null ? 0 : scale, set));

        return target;
    }

    @Override
    public DBCharsetMetadata translate(DBCharsetMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBClusterMetadata translate(DBClusterMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBDatabaseLinkMetadata translate(DBDatabaseLinkMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBDimensionMetadata translate(DBDimensionMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBFunctionMetadata translate(DBFunctionMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBGrantedPrivilegeMetadata translate(DBGrantedPrivilegeMetadata target, ResultSet resultSet)
        throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBGrantedRoleMetadata translate(DBGrantedRoleMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBMaterializedViewMetadata translate(DBMaterializedViewMetadata target, ResultSet resultSet)
        throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBMethodMetadata translate(DBMethodMetadata target, ResultSet resultSet) throws SQLException {
        target.setDeterministic(false);
        target.setOverload(0);
        target.setPosition(0);
        target.setLanguage(null);
        target.setValid(true);
        target.setDebug(false);
        return target;
    }

    @Override
    public DBNestedTableMetadata translate(DBNestedTableMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBNestedTableColumnMetadata translate(DBNestedTableColumnMetadata target, ResultSet resultSet)
        throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBPackageMetadata translate(DBPackageMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBPackageTypeMetadata translate(DBPackageTypeMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBPrivilegeMetadata translate(DBPrivilegeMetadata<? extends DBPrivilegeMetadata> target, ResultSet resultSet)
        throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBProcedureMetadata translate(DBProcedureMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBProgramMetadata translate(DBProgramMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBRoleMetadata translate(DBRoleMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBSequenceMetadata translate(DBSequenceMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBSynonymMetadata translate(DBSynonymMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBTriggerMetadata translate(DBTriggerMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBTypeMetadata translate(DBTypeMetadata target, ResultSet resultSet) throws SQLException {
        String name = resultSet.getString("TYPE_NAME");
        target.setName(name);
        target.setSuperTypeOwner(null);
        target.setSuperTypeName(null);

        String typecode = name;
        target.setCollection(false);
        ConnectionHandler connectionHandler = target.getConnectionHandler();
        target.setNativeDataType(connectionHandler.getObjectBundle().getNativeDataType(typecode));
        return target;
    }

    @Override
    public DBTypeAttributeMetadata translate(DBTypeAttributeMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBUserMetadata translate(DBUserMetadata target, ResultSet resultSet) throws SQLException {
        return super.translate(target, resultSet);
    }

    @Override
    public DBViewMetadata translate(DBViewMetadata target, ResultSet resultSet) throws SQLException {
        target.setName(resultSet.getString("TABLE_NAME"));
        String typeName = resultSet.getString("TABLE_TYPE");
        target.setSystemView(typeName.contains("VIEW") && typeName.contains("SYSTEM"));
        return target;
    }

}
