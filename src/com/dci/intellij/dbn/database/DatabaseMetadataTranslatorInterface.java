package com.dci.intellij.dbn.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.object.metadata.DBArgumentMetadata;
import com.dci.intellij.dbn.object.metadata.DBCharsetMetadata;
import com.dci.intellij.dbn.object.metadata.DBClusterMetadata;
import com.dci.intellij.dbn.object.metadata.DBColumnMetadata;
import com.dci.intellij.dbn.object.metadata.DBConstraintColumnMetadata;
import com.dci.intellij.dbn.object.metadata.DBConstraintMetadata;
import com.dci.intellij.dbn.object.metadata.DBDatabaseLinkMetadata;
import com.dci.intellij.dbn.object.metadata.DBDimensionMetadata;
import com.dci.intellij.dbn.object.metadata.DBFunctionMetadata;
import com.dci.intellij.dbn.object.metadata.DBGrantedPrivilegeMetadata;
import com.dci.intellij.dbn.object.metadata.DBGrantedRoleMetadata;
import com.dci.intellij.dbn.object.metadata.DBIndexMetadata;
import com.dci.intellij.dbn.object.metadata.DBMaterializedViewMetadata;
import com.dci.intellij.dbn.object.metadata.DBMethodMetadata;
import com.dci.intellij.dbn.object.metadata.DBNestedTableColumnMetadata;
import com.dci.intellij.dbn.object.metadata.DBNestedTableMetadata;
import com.dci.intellij.dbn.object.metadata.DBPackageMetadata;
import com.dci.intellij.dbn.object.metadata.DBPackageTypeMetadata;
import com.dci.intellij.dbn.object.metadata.DBPrivilegeMetadata;
import com.dci.intellij.dbn.object.metadata.DBProcedureMetadata;
import com.dci.intellij.dbn.object.metadata.DBProgramMetadata;
import com.dci.intellij.dbn.object.metadata.DBRoleMetadata;
import com.dci.intellij.dbn.object.metadata.DBSchemaMetadata;
import com.dci.intellij.dbn.object.metadata.DBSequenceMetadata;
import com.dci.intellij.dbn.object.metadata.DBSynonymMetadata;
import com.dci.intellij.dbn.object.metadata.DBTableMetadata;
import com.dci.intellij.dbn.object.metadata.DBTriggerMetadata;
import com.dci.intellij.dbn.object.metadata.DBTypeAttributeMetadata;
import com.dci.intellij.dbn.object.metadata.DBTypeMetadata;
import com.dci.intellij.dbn.object.metadata.DBUserMetadata;
import com.dci.intellij.dbn.object.metadata.DBViewMetadata;

/**
 * Database metadata translator interface to isolate resulset processing from DB entities
 *
 * @author Manuel Núñez Sánchez (manuel.nsanchez@gmail.com)
 */
public interface DatabaseMetadataTranslatorInterface {

    /**
     * Extract column dataset name from resulset.
     *
     * @param resultSet
     *     the result set
     * @return the string
     * @throws SQLException
     *     the sql exception
     */
    String columnDatasetName(ResultSet resultSet) throws SQLException;

    /**
     * Extract constraint dataset from resulset.
     *
     * @param resultSet
     *     the result set
     * @return the string
     * @throws SQLException
     *     the sql exception
     */
    String constraintDatasetName(ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db constraint column metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db constraint column metadata
     * @throws SQLException
     *     the sql exception
     */
    DBConstraintColumnMetadata translate(DBConstraintColumnMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db argument metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db argument metadata
     * @throws SQLException
     *     the sql exception
     */
    DBArgumentMetadata translate(DBArgumentMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db charset metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db charset metadata
     * @throws SQLException
     *     the sql exception
     */
    DBCharsetMetadata translate(DBCharsetMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db cluster metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db cluster metadata
     * @throws SQLException
     *     the sql exception
     */
    DBClusterMetadata translate(DBClusterMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db column metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db column metadata
     * @throws SQLException
     *     the sql exception
     */
    DBColumnMetadata translate(DBColumnMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db constraint metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db constraint metadata
     * @throws SQLException
     *     the sql exception
     */
    DBConstraintMetadata translate(DBConstraintMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db database link metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db database link metadata
     * @throws SQLException
     *     the sql exception
     */
    DBDatabaseLinkMetadata translate(DBDatabaseLinkMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db dimension metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db dimension metadata
     * @throws SQLException
     *     the sql exception
     */
    DBDimensionMetadata translate(DBDimensionMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db function metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db function metadata
     * @throws SQLException
     *     the sql exception
     */
    DBFunctionMetadata translate(DBFunctionMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db granted privilege metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db granted privilege metadata
     * @throws SQLException
     *     the sql exception
     */
    DBGrantedPrivilegeMetadata translate(DBGrantedPrivilegeMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db granted role metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db granted role metadata
     * @throws SQLException
     *     the sql exception
     */
    DBGrantedRoleMetadata translate(DBGrantedRoleMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db index metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db index metadata
     * @throws SQLException
     *     the sql exception
     */
    DBIndexMetadata translate(DBIndexMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db materialized view metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db materialized view metadata
     * @throws SQLException
     *     the sql exception
     */
    DBMaterializedViewMetadata translate(DBMaterializedViewMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db method metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db method metadata
     * @throws SQLException
     *     the sql exception
     */
    DBMethodMetadata translate(DBMethodMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db nested table metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db nested table metadata
     * @throws SQLException
     *     the sql exception
     */
    DBNestedTableMetadata translate(DBNestedTableMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db nested table column metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db nested table column metadata
     * @throws SQLException
     *     the sql exception
     */
    DBNestedTableColumnMetadata translate(DBNestedTableColumnMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db package metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db package metadata
     * @throws SQLException
     *     the sql exception
     */
    DBPackageMetadata translate(DBPackageMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db package type metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db package type metadata
     * @throws SQLException
     *     the sql exception
     */
    DBPackageTypeMetadata translate(DBPackageTypeMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db privilege metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db privilege metadata
     * @throws SQLException
     *     the sql exception
     */
    DBPrivilegeMetadata translate(DBPrivilegeMetadata<? extends DBPrivilegeMetadata> target, ResultSet resultSet)
        throws SQLException;

    /**
     * Translate resultSet into db procedure metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db procedure metadata
     * @throws SQLException
     *     the sql exception
     */
    DBProcedureMetadata translate(DBProcedureMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db program metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db program metadata
     * @throws SQLException
     *     the sql exception
     */
    DBProgramMetadata translate(DBProgramMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db role metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db role metadata
     * @throws SQLException
     *     the sql exception
     */
    DBRoleMetadata translate(DBRoleMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db schema metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db schema metadata
     * @throws SQLException
     *     the sql exception
     */
    DBSchemaMetadata translate(DBSchemaMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db sequence metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db sequence metadata
     * @throws SQLException
     *     the sql exception
     */
    DBSequenceMetadata translate(DBSequenceMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db synonym metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db synonym metadata
     * @throws SQLException
     *     the sql exception
     */
    DBSynonymMetadata translate(DBSynonymMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db table metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db table metadata
     * @throws SQLException
     *     the sql exception
     */
    DBTableMetadata translate(DBTableMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db trigger metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db trigger metadata
     * @throws SQLException
     *     the sql exception
     */
    DBTriggerMetadata translate(DBTriggerMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db type metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db type metadata
     * @throws SQLException
     *     the sql exception
     */
    DBTypeMetadata translate(DBTypeMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db type attribute metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db type attribute metadata
     * @throws SQLException
     *     the sql exception
     */
    DBTypeAttributeMetadata translate(DBTypeAttributeMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db user metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db user metadata
     * @throws SQLException
     *     the sql exception
     */
    DBUserMetadata translate(DBUserMetadata target, ResultSet resultSet) throws SQLException;

    /**
     * Translate resultSet into db view metadata.
     *
     * @param target
     *     the target
     * @param resultSet
     *     the result set
     * @return the db view metadata
     * @throws SQLException
     *     the sql exception
     */
    DBViewMetadata translate(DBViewMetadata target, ResultSet resultSet) throws SQLException;

}
