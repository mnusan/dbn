package com.dci.intellij.dbn.object.impl;

import com.dci.intellij.dbn.browser.model.BrowserTreeNode;
import com.dci.intellij.dbn.object.DBNestedTable;
import com.dci.intellij.dbn.object.DBNestedTableColumn;
import com.dci.intellij.dbn.object.common.DBObjectImpl;
import com.dci.intellij.dbn.object.common.DBObjectType;
import com.dci.intellij.dbn.object.metadata.DBNestedTableColumnMetadata;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;
import java.util.List;

public class DBNestedTableColumnImpl extends DBObjectImpl<DBNestedTableColumnMetadata> implements DBNestedTableColumn {

    public DBNestedTableColumnImpl(DBNestedTable parent, DBNestedTableColumnMetadata metadata) throws SQLException {
        super(parent, metadata);
        // todo !!!
    }

    @Override
    protected String initObject(DBNestedTableColumnMetadata metadata) throws SQLException {
        return null; //TODO
    }

    @NotNull
    @Override
    public DBObjectType getObjectType() {
        return DBObjectType.NESTED_TABLE_COLUMN;
    }

    @Override
    public DBNestedTable getNestedTable() {
        return (DBNestedTable) getParentObject();
    }

    /*********************************************************
     *                     TreeElement                       *
     *********************************************************/

    @Override
    public boolean isLeaf() {
        return true;
    }

    @Override
    @NotNull
    public List<BrowserTreeNode> buildAllPossibleTreeChildren() {
        return EMPTY_TREE_NODE_LIST;
    }
}
