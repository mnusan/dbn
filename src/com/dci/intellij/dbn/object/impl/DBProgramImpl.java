package com.dci.intellij.dbn.object.impl;

import com.dci.intellij.dbn.editor.DBContentType;
import com.dci.intellij.dbn.object.DBFunction;
import com.dci.intellij.dbn.object.DBMethod;
import com.dci.intellij.dbn.object.DBProcedure;
import com.dci.intellij.dbn.object.DBProgram;
import com.dci.intellij.dbn.object.DBSchema;
import com.dci.intellij.dbn.object.common.DBSchemaObject;
import com.dci.intellij.dbn.object.common.DBSchemaObjectImpl;
import com.dci.intellij.dbn.object.common.list.DBObjectList;
import com.dci.intellij.dbn.object.common.status.DBObjectStatus;
import com.dci.intellij.dbn.object.common.status.DBObjectStatusHolder;
import com.dci.intellij.dbn.object.metadata.DBProgramMetadata;

import java.sql.SQLException;
import java.util.List;

import static com.dci.intellij.dbn.object.common.property.DBObjectProperty.*;

public abstract class DBProgramImpl<P extends DBProcedure, F extends DBFunction, O extends DBProgramMetadata<O>>
        extends DBSchemaObjectImpl<O> implements DBProgram<P, F> {
    protected DBObjectList<P> procedures;
    protected DBObjectList<F> functions;


     DBProgramImpl(DBSchemaObject parent, O metadata) throws SQLException {
        super(parent, metadata);
    }

     DBProgramImpl(DBSchema schema, O metadata) throws SQLException {
        super(schema, metadata);
    }

    @Override
    public void initProperties() {
        super.initProperties();
        properties.set(INVALIDABLE, true);
        properties.set(COMPILABLE, true);
        properties.set(DEBUGABLE, true);
    }

    @Override
    protected abstract String initObject(O metadata) throws SQLException;

    @Override
    public void initStatus(O metadata) throws SQLException {
        DBObjectStatusHolder objectStatus = getStatus();

        objectStatus.set(DBContentType.CODE_SPEC, DBObjectStatus.PRESENT, metadata.isSpecPresent());
        objectStatus.set(DBContentType.CODE_SPEC, DBObjectStatus.VALID, metadata.isSpecValid());
        objectStatus.set(DBContentType.CODE_SPEC, DBObjectStatus.DEBUG, metadata.isSpecDebug());

        objectStatus.set(DBContentType.CODE_BODY, DBObjectStatus.PRESENT, metadata.isBodyPresent());
        objectStatus.set(DBContentType.CODE_BODY, DBObjectStatus.VALID, metadata.isBodyValid());
        objectStatus.set(DBContentType.CODE_BODY, DBObjectStatus.DEBUG, metadata.isBodyDebug());

    }

    @Override
    public List<F> getFunctions() {
        return functions.getObjects();
    }

    @Override
    public List<P> getProcedures() {
        return procedures.getObjects();
    }

    @Override
    public F getFunction(String name, int overload) {
        for (F function : functions.getObjects()){
            if (function.getName().equals(name) && function.getOverload() == overload) {
                return function;
            }
        }
        return null;
    }

    @Override
    public P getProcedure(String name, int overload) {
        for (P procedure : procedures.getObjects()){
            if (procedure.getName().equals(name) && procedure.getOverload() == overload) {
                return procedure;
            }
        }
        return null;
    }

    @Override
    public DBMethod getMethod(String name, int overload) {
        DBMethod method = getProcedure(name, overload);
        if (method == null) method = getFunction(name, overload);
        return method;
    }

    @Override
    public boolean isEmbedded() {
        return false;
    }

    @Override
    public boolean isEditable(DBContentType contentType) {
        return getContentType() == DBContentType.CODE_SPEC_AND_BODY && (
                contentType == DBContentType.CODE_SPEC ||
                contentType == DBContentType.CODE_BODY);
    }
}
