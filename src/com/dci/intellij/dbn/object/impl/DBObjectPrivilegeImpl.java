package com.dci.intellij.dbn.object.impl;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.DBObjectPrivilege;
import com.dci.intellij.dbn.object.common.DBObjectType;
import com.dci.intellij.dbn.object.metadata.DBObjectPrivilegeMetadata;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;

public class DBObjectPrivilegeImpl extends DBPrivilegeImpl<DBObjectPrivilegeMetadata> implements DBObjectPrivilege {

    public DBObjectPrivilegeImpl(ConnectionHandler connectionHandler, DBObjectPrivilegeMetadata metadata) throws SQLException {
        super(connectionHandler, metadata);
    }

    @NotNull
    @Override
    public DBObjectType getObjectType() {
        return DBObjectType.OBJECT_PRIVILEGE;
    }

}
