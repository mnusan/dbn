package com.dci.intellij.dbn.object.impl;

import com.dci.intellij.dbn.browser.model.BrowserTreeNode;
import com.dci.intellij.dbn.browser.ui.HtmlToolTipBuilder;
import com.dci.intellij.dbn.object.DBCluster;
import com.dci.intellij.dbn.object.DBSchema;
import com.dci.intellij.dbn.object.common.DBObjectType;
import com.dci.intellij.dbn.object.common.DBSchemaObjectImpl;
import com.dci.intellij.dbn.object.metadata.DBClusterMetadata;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;
import java.util.List;

public class DBClusterImpl extends DBSchemaObjectImpl<DBClusterMetadata> implements DBCluster {
    DBClusterImpl(DBSchema parent, DBClusterMetadata metadata) throws SQLException {
        super(parent, metadata);
    }

    @Override
    protected String initObject(DBClusterMetadata metadata) throws SQLException {
        return metadata.getName();
    }

    @NotNull
    @Override
    public DBObjectType getObjectType() {
        return DBObjectType.CLUSTER;
    }

    @Override
    public void buildToolTip(HtmlToolTipBuilder ttb) {
        ttb.append(true, getObjectType().getName(), true);
        ttb.createEmptyRow();
        super.buildToolTip(ttb);
    }

    /*********************************************************
     *                     TreeElement                       *
     *********************************************************/

    @Override
    public boolean isLeaf() {
        return true;
    }

    @Override
    @NotNull
    public List<BrowserTreeNode> buildAllPossibleTreeChildren() {
        return EMPTY_TREE_NODE_LIST;
    }
}
