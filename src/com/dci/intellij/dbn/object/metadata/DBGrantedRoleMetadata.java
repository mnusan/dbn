package com.dci.intellij.dbn.object.metadata;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBGrantedRoleMetadata extends DBObjectMetadata<DBGrantedRoleMetadata> {

    boolean isAdmin();

    DBGrantedRoleMetadata setAdmin(boolean admin);

    boolean isDefaultRole();

    DBGrantedRoleMetadata setDefaultRole(boolean defaultRole);

}
