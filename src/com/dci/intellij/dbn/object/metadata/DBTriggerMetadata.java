package com.dci.intellij.dbn.object.metadata;

import com.dci.intellij.dbn.object.DBTrigger.TriggerType;
import com.dci.intellij.dbn.object.DBTrigger.TriggeringEvent;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBTriggerMetadata extends DBObjectMetadata<DBTriggerMetadata> {

    boolean isForEachRow();

    DBTriggerMetadata setForEachRow(boolean forEachRow);

    TriggerType getTriggerType();

    DBTriggerMetadata setTriggerType(TriggerType triggerType);

    TriggeringEvent[] getTriggeringEvents();

    DBTriggerMetadata setTriggeringEvents(TriggeringEvent[] triggeringEvents);

    boolean isEnabled();

    DBTriggerMetadata setEnabled(boolean enabled);

    boolean isValid();

    DBTriggerMetadata setValid(boolean valid);

    boolean isDebug();

    DBTriggerMetadata setDebug(boolean debug);

}
