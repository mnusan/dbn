package com.dci.intellij.dbn.object.metadata;

import com.dci.intellij.dbn.object.common.DBObject;
import com.dci.intellij.dbn.object.lookup.DBObjectRef;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBSynonymMetadata extends DBObjectMetadata<DBSynonymMetadata> {

    boolean isValid();

    DBSynonymMetadata setValid(boolean valid);

    DBObjectRef<DBObject> getUnderlyingObject();

    DBSynonymMetadata setUnderlyingObject(DBObjectRef<DBObject> underlyingObject);

}
