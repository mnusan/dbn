package com.dci.intellij.dbn.object.metadata;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBDatasetMetadata<O extends DBDatasetMetadata> extends DBObjectMetadata<O> {}
