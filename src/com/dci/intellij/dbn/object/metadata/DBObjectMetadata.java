package com.dci.intellij.dbn.object.metadata;

import com.dci.intellij.dbn.connection.ConnectionHandler;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBObjectMetadata<O extends DBObjectMetadata> {

    ConnectionHandler getConnectionHandler();

    String getName();

    O setName(String name);

}
