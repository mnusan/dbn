package com.dci.intellij.dbn.object.metadata;

import com.dci.intellij.dbn.object.metadata.impl.DBConstraintColumnMetadataImpl;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBConstraintColumnMetadata extends DBObjectMetadata<DBConstraintColumnMetadata> {

    String getColumnName();

    DBConstraintColumnMetadataImpl setColumnName(String columnName);

    String getConstraintName();

    DBConstraintColumnMetadataImpl setConstraintName(String constraintName);

    int getPosition();

    DBConstraintColumnMetadataImpl setPosition(int position);

}
