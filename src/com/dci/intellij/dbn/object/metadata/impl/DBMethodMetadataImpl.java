package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.language.common.DBLanguage;
import com.dci.intellij.dbn.object.metadata.DBMethodMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public abstract class DBMethodMetadataImpl<O extends DBMethodMetadata> extends DBObjectMetadataImpl<O>
    implements DBMethodMetadata<O> {

    protected int position;
    protected int overload;
    private DBLanguage language;
    boolean valid;
    boolean debug;
    boolean deterministic;

    public DBMethodMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected O translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        return (O)connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public O setPosition(int position) {
        this.position = position;
        return (O)this;
    }

    @Override
    public int getOverload() {
        return overload;
    }

    @Override
    public O setOverload(int overload) {
        this.overload = overload;
        return (O)this;
    }

    @Override
    public DBLanguage getLanguage() {
        return language;
    }

    @Override
    public O setLanguage(DBLanguage language) {
        this.language = language;
        return (O)this;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
    public O setValid(boolean valid) {
        this.valid = valid;
        return (O)this;
    }

    @Override
    public boolean isDebug() {
        return debug;
    }

    @Override
    public O setDebug(boolean debug) {
        this.debug = debug;
        return (O)this;
    }

    @Override
    public boolean isDeterministic() {
        return deterministic;
    }

    @Override
    public O setDeterministic(boolean deterministic) {
        this.deterministic = deterministic;
        return (O)this;
    }

}
