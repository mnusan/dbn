package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBSchemaMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBSchemaMetadataImpl extends DBObjectMetadataImpl<DBSchemaMetadata> implements DBSchemaMetadata {

    private boolean pub;
    private boolean system;
    private boolean empty;

    public DBSchemaMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBSchemaMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public boolean isPublic() {
        return pub;
    }

    @Override
    public DBSchemaMetadata setPublic(boolean pub) {
        this.pub = pub;
        return this;
    }

    @Override
    public boolean isSystem() {
        return system;
    }

    @Override
    public DBSchemaMetadata setSystem(boolean system) {
        this.system = system;
        return this;
    }

    @Override
    public boolean isEmpty() {
        return empty;
    }

    @Override
    public DBSchemaMetadata setEmpty(boolean empty) {
        this.empty = empty;
        return this;
    }

}
