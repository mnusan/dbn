package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBRoleMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBRoleMetadataImpl extends DBObjectMetadataImpl<DBRoleMetadata> implements DBRoleMetadata {

    public DBRoleMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBRoleMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

}
