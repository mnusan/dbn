package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.DBTrigger.TriggerType;
import com.dci.intellij.dbn.object.DBTrigger.TriggeringEvent;
import com.dci.intellij.dbn.object.metadata.DBTriggerMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBTriggerMetadataImpl extends DBObjectMetadataImpl<DBTriggerMetadata> implements DBTriggerMetadata {

    private boolean forEachRow;
    private TriggerType triggerType;
    private TriggeringEvent[] triggeringEvents;
    boolean enabled;
    boolean valid;
    boolean debug;

    public DBTriggerMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBTriggerMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public boolean isForEachRow() {
        return forEachRow;
    }

    @Override
    public DBTriggerMetadata setForEachRow(boolean forEachRow) {
        this.forEachRow = forEachRow;
        return this;
    }

    @Override
    public TriggerType getTriggerType() {
        return triggerType;
    }

    @Override
    public DBTriggerMetadata setTriggerType(TriggerType triggerType) {
        this.triggerType = triggerType;
        return this;
    }

    @Override
    public TriggeringEvent[] getTriggeringEvents() {
        return triggeringEvents;
    }

    @Override
    public DBTriggerMetadata setTriggeringEvents(TriggeringEvent[] triggeringEvents) {
        this.triggeringEvents = triggeringEvents;
        return this;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public DBTriggerMetadata setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
    public DBTriggerMetadata setValid(boolean valid) {
        this.valid = valid;
        return this;
    }

    @Override
    public boolean isDebug() {
        return debug;
    }

    @Override
    public DBTriggerMetadata setDebug(boolean debug) {
        this.debug = debug;
        return this;
    }

}
