package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBIndexMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBIndexMetadataImpl extends DBObjectMetadataImpl<DBIndexMetadata> implements DBIndexMetadata {

    private boolean unique;
    private boolean valid;

    public DBIndexMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBIndexMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public boolean isUnique() {
        return unique;
    }

    @Override
    public DBIndexMetadata setUnique(boolean unique) {
        this.unique = unique;
        return this;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
    public DBIndexMetadata setValid(boolean valid) {
        this.valid = valid;
        return this;
    }

}
