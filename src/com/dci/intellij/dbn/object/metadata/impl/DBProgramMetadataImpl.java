package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBProgramMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public abstract class DBProgramMetadataImpl<O extends DBProgramMetadata> extends DBObjectMetadataImpl<O>
    implements DBProgramMetadata<O> {

    boolean specPresent;
    boolean specValid;
    boolean specDebug;
    boolean bodyPresent;
    boolean bodyValid;
    boolean bodyDebug;

    public DBProgramMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected O translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        return (O)connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public boolean isSpecPresent() {
        return specPresent;
    }

    @Override
    public O setSpecPresent(boolean specPresent) {
        this.specPresent = specPresent;
        return (O)this;
    }

    @Override
    public boolean isSpecValid() {
        return specValid;
    }

    @Override
    public O setSpecValid(boolean specValid) {
        this.specValid = specValid;
        return (O)this;
    }

    @Override
    public boolean isSpecDebug() {
        return specDebug;
    }

    @Override
    public O setSpecDebug(boolean specDebug) {
        this.specDebug = specDebug;
        return (O)this;
    }

    @Override
    public boolean isBodyPresent() {
        return bodyPresent;
    }

    @Override
    public O setBodyPresent(boolean bodyPresent) {
        this.bodyPresent = bodyPresent;
        return (O)this;
    }

    @Override
    public boolean isBodyValid() {
        return bodyValid;
    }

    @Override
    public O setBodyValid(boolean bodyValid) {
        this.bodyValid = bodyValid;
        return (O)this;
    }

    @Override
    public boolean isBodyDebug() {
        return bodyDebug;
    }

    @Override
    public O setBodyDebug(boolean bodyDebug) {
        this.bodyDebug = bodyDebug;
        return (O)this;
    }

}
