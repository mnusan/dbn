package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.DBType;
import com.dci.intellij.dbn.object.lookup.DBObjectRef;
import com.dci.intellij.dbn.object.metadata.DBNestedTableMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBNestedTableMetadataImpl extends DBObjectMetadataImpl<DBNestedTableMetadata>
    implements DBNestedTableMetadata {

    private DBObjectRef<DBType> typeRef;

    public DBNestedTableMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBNestedTableMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public DBObjectRef<DBType> getTypeRef() {
        return typeRef;
    }

    @Override
    public DBNestedTableMetadata setTypeRef(DBObjectRef<DBType> typeRef) {
        this.typeRef = typeRef;
        return this;
    }

}
