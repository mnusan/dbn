package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.DBConstraint;
import com.dci.intellij.dbn.object.lookup.DBObjectRef;
import com.dci.intellij.dbn.object.metadata.DBConstraintMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBConstraintMetadataImpl extends DBObjectMetadataImpl<DBConstraintMetadata>
    implements DBConstraintMetadata {

    private int constraintType;
    private DBObjectRef<DBConstraint> foreignKeyConstraint;
    private String checkCondition;
    private boolean enabled;

    public DBConstraintMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBConstraintMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public int getConstraintType() {
        return constraintType;
    }

    @Override
    public DBConstraintMetadata setConstraintType(int constraintType) {
        this.constraintType = constraintType;
        return this;
    }

    @Override
    public DBObjectRef<DBConstraint> getForeignKeyConstraint() {
        return foreignKeyConstraint;
    }

    @Override
    public DBConstraintMetadata setForeignKeyConstraint(DBObjectRef<DBConstraint> foreignKeyConstraint) {
        this.foreignKeyConstraint = foreignKeyConstraint;
        return this;
    }

    @Override
    public String getCheckCondition() {
        return checkCondition;
    }

    @Override
    public DBConstraintMetadata setCheckCondition(String checkCondition) {
        this.checkCondition = checkCondition;
        return this;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public DBConstraintMetadata setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

}
