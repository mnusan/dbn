package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.DBType;
import com.dci.intellij.dbn.object.metadata.DBViewMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBViewMetadataImpl<O extends DBViewMetadata> extends DBObjectMetadataImpl<O> implements DBViewMetadata<O> {

    private boolean systemView;
    private DBType type;

    public DBViewMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected O translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        return (O)connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public boolean isSystemView() {
        return systemView;
    }

    @Override
    public O setSystemView(boolean systemView) {
        this.systemView = systemView;
        return (O)this;
    }

    @Override
    public DBType getType() {
        return type;
    }

    @Override
    public O setType(DBType type) {
        this.type = type;
        return (O)this;
    }

}
