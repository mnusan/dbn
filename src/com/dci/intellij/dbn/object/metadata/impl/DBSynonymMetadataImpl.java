package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.common.DBObject;
import com.dci.intellij.dbn.object.lookup.DBObjectRef;
import com.dci.intellij.dbn.object.metadata.DBSynonymMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBSynonymMetadataImpl extends DBObjectMetadataImpl<DBSynonymMetadata> implements DBSynonymMetadata {

    boolean valid;
    private DBObjectRef<DBObject> underlyingObject;

    public DBSynonymMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBSynonymMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
    public DBSynonymMetadata setValid(boolean valid) {
        this.valid = valid;
        return this;
    }

    @Override
    public DBObjectRef<DBObject> getUnderlyingObject() {
        return underlyingObject;
    }

    @Override
    public DBSynonymMetadata setUnderlyingObject(DBObjectRef<DBObject> underlyingObject) {
        this.underlyingObject = underlyingObject;
        return this;
    }

}
