package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBDatabaseLinkMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBDatabaseLinkMetadataImpl extends DBObjectMetadataImpl<DBDatabaseLinkMetadata>
    implements DBDatabaseLinkMetadata {

    private String userName;
    private String host;

    public DBDatabaseLinkMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBDatabaseLinkMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public DBDatabaseLinkMetadataImpl setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public DBDatabaseLinkMetadataImpl setHost(String host) {
        this.host = host;
        return this;
    }

}
