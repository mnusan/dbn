package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.data.type.DBDataType;
import com.dci.intellij.dbn.object.metadata.DBColumnMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBColumnMetadataImpl extends DBObjectMetadataImpl<DBColumnMetadata> implements DBColumnMetadata {

    private DBDataType dataType;
    private int position;
    private boolean nullable;
    private boolean primaryKey;
    private boolean foreignKey;
    private boolean uniqueKey;
    private boolean hidden;

    public DBColumnMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBColumnMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public DBDataType getDataType() {
        return dataType;
    }

    @Override
    public DBColumnMetadataImpl setDataType(DBDataType dataType) {
        this.dataType = dataType;
        return this;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public DBColumnMetadataImpl setPosition(int position) {
        this.position = position;
        return this;
    }

    @Override
    public boolean isNullable() {
        return nullable;
    }

    @Override
    public DBColumnMetadataImpl setNullable(boolean nullable) {
        this.nullable = nullable;
        return this;
    }

    @Override
    public boolean isPrimaryKey() {
        return primaryKey;
    }

    @Override
    public DBColumnMetadataImpl setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
        return this;
    }

    @Override
    public boolean isForeignKey() {
        return foreignKey;
    }

    @Override
    public DBColumnMetadataImpl setForeignKey(boolean foreignKey) {
        this.foreignKey = foreignKey;
        return this;
    }

    @Override
    public boolean isUniqueKey() {
        return uniqueKey;
    }

    @Override
    public DBColumnMetadataImpl setUniqueKey(boolean uniqueKey) {
        this.uniqueKey = uniqueKey;
        return this;
    }

    @Override
    public boolean isHidden() {
        return hidden;
    }

    @Override
    public DBColumnMetadataImpl setHidden(boolean hidden) {
        this.hidden = hidden;
        return this;
    }

}
