package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.data.type.DBDataType;
import com.dci.intellij.dbn.object.metadata.DBTypeAttributeMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBTypeAttributeMetadataImpl extends DBObjectMetadataImpl<DBTypeAttributeMetadata>
    implements DBTypeAttributeMetadata {

    private DBDataType dataType;
    private int position;

    public DBTypeAttributeMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBTypeAttributeMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public DBDataType getDataType() {
        return dataType;
    }

    @Override
    public DBTypeAttributeMetadata setDataType(DBDataType dataType) {
        this.dataType = dataType;
        return this;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public DBTypeAttributeMetadata setPosition(int position) {
        this.position = position;
        return this;
    }

}
