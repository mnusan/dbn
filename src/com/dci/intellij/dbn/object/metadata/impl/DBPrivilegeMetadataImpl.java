package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBPrivilegeMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public abstract class DBPrivilegeMetadataImpl<O extends DBPrivilegeMetadata> extends DBObjectMetadataImpl<O>
    implements DBPrivilegeMetadata<O> {

    public DBPrivilegeMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected O translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        return (O)connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

}
