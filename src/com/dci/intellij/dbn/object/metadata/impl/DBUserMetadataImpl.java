package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBUserMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBUserMetadataImpl extends DBObjectMetadataImpl<DBUserMetadata> implements DBUserMetadata {

    private boolean expired;
    private boolean locked;

    public DBUserMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBUserMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public boolean isExpired() {
        return expired;
    }

    @Override
    public DBUserMetadata setExpired(boolean expired) {
        this.expired = expired;
        return this;
    }

    @Override
    public boolean isLocked() {
        return locked;
    }

    @Override
    public DBUserMetadata setLocked(boolean locked) {
        this.locked = locked;
        return this;
    }

}
