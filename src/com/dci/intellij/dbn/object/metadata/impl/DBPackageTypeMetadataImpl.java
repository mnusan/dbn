package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBPackageTypeMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBPackageTypeMetadataImpl extends DBTypeMetadataImpl<DBPackageTypeMetadata>
    implements DBPackageTypeMetadata {

    public DBPackageTypeMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBPackageTypeMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        super.translate(connectionHandler, resultSet);
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

}
