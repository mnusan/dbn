package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBConstraintColumnMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBConstraintColumnMetadataImpl extends DBObjectMetadataImpl<DBConstraintColumnMetadata>
    implements DBConstraintColumnMetadata {

    private String columnName;
    private String constraintName;
    private int position;

    public DBConstraintColumnMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBConstraintColumnMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public String getColumnName() {
        return columnName;
    }

    @Override
    public DBConstraintColumnMetadataImpl setColumnName(String columnName) {
        this.columnName = columnName;
        return this;
    }

    @Override
    public String getConstraintName() {
        return constraintName;
    }

    @Override
    public DBConstraintColumnMetadataImpl setConstraintName(String constraintName) {
        this.constraintName = constraintName;
        return this;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public DBConstraintColumnMetadataImpl setPosition(int position) {
        this.position = position;
        return this;
    }

}
