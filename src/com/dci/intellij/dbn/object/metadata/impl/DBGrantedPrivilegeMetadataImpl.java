package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBGrantedPrivilegeMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBGrantedPrivilegeMetadataImpl extends DBObjectMetadataImpl<DBGrantedPrivilegeMetadata>
    implements DBGrantedPrivilegeMetadata {

    private boolean admin;

    public DBGrantedPrivilegeMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBGrantedPrivilegeMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public boolean isAdmin() {
        return admin;
    }

    @Override
    public DBGrantedPrivilegeMetadataImpl setAdmin(boolean admin) {
        this.admin = admin;
        return this;
    }

}
