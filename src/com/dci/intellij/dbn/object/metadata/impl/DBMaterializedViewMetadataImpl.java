package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBMaterializedViewMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBMaterializedViewMetadataImpl extends DBViewMetadataImpl<DBMaterializedViewMetadata>
    implements com.dci.intellij.dbn.object.metadata.DBMaterializedViewMetadata {

    public DBMaterializedViewMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBMaterializedViewMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        super.translate(connectionHandler, resultSet);
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

}
