package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBNestedTableColumnMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBNestedTableColumnMetadataImpl extends DBObjectMetadataImpl<DBNestedTableColumnMetadata>
    implements DBNestedTableColumnMetadata {

    public DBNestedTableColumnMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBNestedTableColumnMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

}
