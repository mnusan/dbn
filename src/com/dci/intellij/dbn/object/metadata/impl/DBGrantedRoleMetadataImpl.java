package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBGrantedRoleMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBGrantedRoleMetadataImpl extends DBObjectMetadataImpl<DBGrantedRoleMetadata>
    implements DBGrantedRoleMetadata {

    private boolean admin;
    private boolean defaultRole;

    public DBGrantedRoleMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBGrantedRoleMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public boolean isAdmin() {
        return admin;
    }

    @Override
    public DBGrantedRoleMetadata setAdmin(boolean admin) {
        this.admin = admin;
        return this;
    }

    @Override
    public boolean isDefaultRole() {
        return defaultRole;
    }

    @Override
    public DBGrantedRoleMetadata setDefaultRole(boolean defaultRole) {
        this.defaultRole = defaultRole;
        return this;
    }

}
