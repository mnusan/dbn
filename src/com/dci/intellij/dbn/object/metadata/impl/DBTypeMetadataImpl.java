package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.data.type.DBDataType;
import com.dci.intellij.dbn.data.type.DBDataType.Ref;
import com.dci.intellij.dbn.data.type.DBNativeDataType;
import com.dci.intellij.dbn.object.metadata.DBTypeMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBTypeMetadataImpl<O extends DBTypeMetadata> extends DBProgramMetadataImpl<O>
    implements DBTypeMetadata<O> {

    private String superTypeOwner;
    private String superTypeName;
    private boolean collection;
    private DBNativeDataType nativeDataType;
    private DBDataType.Ref collectionElementTypeRef;

    public DBTypeMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected O translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        return (O)connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public String getSuperTypeOwner() {
        return superTypeOwner;
    }

    @Override
    public O setSuperTypeOwner(String superTypeOwner) {
        this.superTypeOwner = superTypeOwner;
        return (O)this;
    }

    @Override
    public String getSuperTypeName() {
        return superTypeName;
    }

    @Override
    public O setSuperTypeName(String superTypeName) {
        this.superTypeName = superTypeName;
        return (O)this;
    }

    @Override
    public boolean isCollection() {
        return collection;
    }

    @Override
    public O setCollection(boolean collection) {
        this.collection = collection;
        return (O)this;
    }

    @Override
    public DBNativeDataType getNativeDataType() {
        return nativeDataType;
    }

    @Override
    public O setNativeDataType(DBNativeDataType nativeDataType) {
        this.nativeDataType = nativeDataType;
        return (O)this;
    }

    @Override
    public Ref getCollectionElementTypeRef() {
        return collectionElementTypeRef;
    }

    @Override
    public O setCollectionElementTypeRef(Ref collectionElementTypeRef) {
        this.collectionElementTypeRef = collectionElementTypeRef;
        return (O)this;
    }

}
