package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBSystemPrivilegeMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBSystemPrivilegeMetadataImpl extends DBPrivilegeMetadataImpl<DBSystemPrivilegeMetadata>
    implements DBSystemPrivilegeMetadata {

    public DBSystemPrivilegeMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBSystemPrivilegeMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
        return this;
    }

}
