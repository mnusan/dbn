package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.data.type.DBDataType;
import com.dci.intellij.dbn.object.metadata.DBArgumentMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBArgumentMetadataImpl extends DBObjectMetadataImpl<DBArgumentMetadata> implements DBArgumentMetadata {

    private DBDataType dataType;
    private int overload;
    private int position;
    private int sequence;
    public boolean input;
    public boolean output;

    public DBArgumentMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBArgumentMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public DBDataType getDataType() {
        return dataType;
    }

    @Override
    public DBArgumentMetadata setDataType(DBDataType dataType) {
        this.dataType = dataType;
        return this;
    }

    @Override
    public int getOverload() {
        return overload;
    }

    @Override
    public DBArgumentMetadata setOverload(int overload) {
        this.overload = overload;
        return this;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public DBArgumentMetadata setPosition(int position) {
        this.position = position;
        return this;
    }

    @Override
    public int getSequence() {
        return sequence;
    }

    @Override
    public DBArgumentMetadata setSequence(int sequence) {
        this.sequence = sequence;
        return this;
    }

    @Override
    public boolean isInput() {
        return input;
    }

    @Override
    public DBArgumentMetadata setInput(boolean input) {
        this.input = input;
        return this;
    }

    @Override
    public boolean isOutput() {
        return output;
    }

    @Override
    public DBArgumentMetadata setOutput(boolean output) {
        this.output = output;
        return this;
    }

}
