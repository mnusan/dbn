package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBObjectMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public abstract class DBObjectMetadataImpl<O extends DBObjectMetadata> implements DBObjectMetadata<O> {

    private String name;
    private final ConnectionHandler connectionHandler;

    public DBObjectMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        this.connectionHandler = connectionHandler;
        translate(connectionHandler, resultSet);
    }

    @Override
    public ConnectionHandler getConnectionHandler() {
        return connectionHandler;
    }

    protected abstract O translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public O setName(String name) {
        this.name = name;
        return (O)this;
    }

}
