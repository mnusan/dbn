package com.dci.intellij.dbn.object.metadata.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.dci.intellij.dbn.connection.ConnectionHandler;
import com.dci.intellij.dbn.object.metadata.DBCharsetMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public class DBCharsetMetadataImpl extends DBObjectMetadataImpl<DBCharsetMetadata> implements DBCharsetMetadata {

    private int maxLength;

    public DBCharsetMetadataImpl(ConnectionHandler connectionHandler, ResultSet resultSet) throws SQLException {
        super(connectionHandler, resultSet);
    }

    @Override
    protected DBCharsetMetadata translate(@NotNull ConnectionHandler connectionHandler, ResultSet resultSet)
        throws SQLException {
        return connectionHandler.getInterfaceProvider().getMetadataTranslatorInterface().translate(this, resultSet);
    }

    @Override
    public int getMaxLength() {
        return maxLength;
    }

    @Override
    public DBCharsetMetadata setMaxLength(int maxLength) {
        this.maxLength = maxLength;
        return this;
    }

}
