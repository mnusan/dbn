package com.dci.intellij.dbn.object.metadata;

import com.dci.intellij.dbn.data.type.DBDataType.Ref;
import com.dci.intellij.dbn.data.type.DBNativeDataType;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBTypeMetadata<O extends DBTypeMetadata> extends DBProgramMetadata<O> {

    String getSuperTypeOwner();

    O setSuperTypeOwner(String superTypeOwner);

    String getSuperTypeName();

    O setSuperTypeName(String superTypeName);

    boolean isCollection();

    O setCollection(boolean collection);

    DBNativeDataType getNativeDataType();

    O setNativeDataType(DBNativeDataType nativeDataType);

    Ref getCollectionElementTypeRef();

    O setCollectionElementTypeRef(Ref collectionElementTypeRef);

}
