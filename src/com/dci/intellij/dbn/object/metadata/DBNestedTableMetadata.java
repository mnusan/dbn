package com.dci.intellij.dbn.object.metadata;

import com.dci.intellij.dbn.object.DBType;
import com.dci.intellij.dbn.object.lookup.DBObjectRef;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBNestedTableMetadata extends DBObjectMetadata<DBNestedTableMetadata> {

    DBObjectRef<DBType> getTypeRef();

    DBNestedTableMetadata setTypeRef(DBObjectRef<DBType> typeRef);

}
