package com.dci.intellij.dbn.object.metadata;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBCharsetMetadata extends DBObjectMetadata<DBCharsetMetadata> {

    int getMaxLength();

    DBCharsetMetadata setMaxLength(int maxLength);

}
