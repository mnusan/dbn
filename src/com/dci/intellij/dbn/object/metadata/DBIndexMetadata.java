package com.dci.intellij.dbn.object.metadata;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBIndexMetadata extends DBObjectMetadata<DBIndexMetadata> {

    boolean isUnique();

    DBIndexMetadata setUnique(boolean unique);

    boolean isValid();

    DBIndexMetadata setValid(boolean valid);

}
