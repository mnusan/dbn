package com.dci.intellij.dbn.object.metadata;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBTableMetadata extends DBObjectMetadata<DBTableMetadata> {

    boolean isTemporary();

    DBTableMetadata setTemporary(boolean temporary);

}
