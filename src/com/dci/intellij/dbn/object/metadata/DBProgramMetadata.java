package com.dci.intellij.dbn.object.metadata;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBProgramMetadata<O extends DBProgramMetadata> extends DBObjectMetadata<O> {

    boolean isSpecPresent();

    O setSpecPresent(boolean specPresent);

    boolean isSpecValid();

    O setSpecValid(boolean specValid);

    boolean isSpecDebug();

    O setSpecDebug(boolean specDebug);

    boolean isBodyPresent();

    O setBodyPresent(boolean bodyPresent);

    boolean isBodyValid();

    O setBodyValid(boolean bodyValid);

    boolean isBodyDebug();

    O setBodyDebug(boolean bodyDebug);

}
