package com.dci.intellij.dbn.object.metadata;

import com.dci.intellij.dbn.data.type.DBDataType;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBTypeAttributeMetadata extends DBObjectMetadata<DBTypeAttributeMetadata> {

    DBDataType getDataType();

    DBTypeAttributeMetadata setDataType(DBDataType dataType);

    int getPosition();

    DBTypeAttributeMetadata setPosition(int position);

}
