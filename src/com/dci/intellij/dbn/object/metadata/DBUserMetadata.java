package com.dci.intellij.dbn.object.metadata;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBUserMetadata extends DBObjectMetadata<DBUserMetadata> {

    boolean isExpired();

    DBUserMetadata setExpired(boolean expired);

    boolean isLocked();

    DBUserMetadata setLocked(boolean locked);

}
