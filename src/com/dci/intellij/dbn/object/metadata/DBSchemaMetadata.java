package com.dci.intellij.dbn.object.metadata;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBSchemaMetadata extends DBObjectMetadata<DBSchemaMetadata> {

    boolean isPublic();

    DBSchemaMetadata setPublic(boolean pub);

    boolean isSystem();

    DBSchemaMetadata setSystem(boolean system);

    boolean isEmpty();

    DBSchemaMetadata setEmpty(boolean empty);

}
