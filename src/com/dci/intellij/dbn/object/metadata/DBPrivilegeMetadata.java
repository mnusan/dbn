package com.dci.intellij.dbn.object.metadata;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBPrivilegeMetadata<O extends DBPrivilegeMetadata> extends DBObjectMetadata<O> {}
