package com.dci.intellij.dbn.object.metadata;

import com.dci.intellij.dbn.object.DBConstraint;
import com.dci.intellij.dbn.object.lookup.DBObjectRef;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBConstraintMetadata extends DBObjectMetadata<DBConstraintMetadata> {

    int getConstraintType();

    DBConstraintMetadata setConstraintType(int constraintType);

    DBObjectRef<DBConstraint> getForeignKeyConstraint();

    DBConstraintMetadata setForeignKeyConstraint(DBObjectRef<DBConstraint> foreignKeyConstraint);

    String getCheckCondition();

    DBConstraintMetadata setCheckCondition(String checkCondition);

    boolean isEnabled();

    DBConstraintMetadata setEnabled(boolean enabled);

}
