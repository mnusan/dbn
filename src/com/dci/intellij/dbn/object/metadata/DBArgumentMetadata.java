package com.dci.intellij.dbn.object.metadata;

import com.dci.intellij.dbn.data.type.DBDataType;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBArgumentMetadata extends DBObjectMetadata<DBArgumentMetadata> {

    DBDataType getDataType();

    DBArgumentMetadata setDataType(DBDataType dataType);

    int getOverload();

    DBArgumentMetadata setOverload(int overload);

    int getPosition();

    DBArgumentMetadata setPosition(int position);

    int getSequence();

    DBArgumentMetadata setSequence(int sequence);

    boolean isInput();

    DBArgumentMetadata setInput(boolean input);

    boolean isOutput();

    DBArgumentMetadata setOutput(boolean output);

}
