package com.dci.intellij.dbn.object.metadata;

import com.dci.intellij.dbn.object.metadata.impl.DBGrantedPrivilegeMetadataImpl;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBGrantedPrivilegeMetadata extends DBObjectMetadata<DBGrantedPrivilegeMetadata> {

    boolean isAdmin();

    DBGrantedPrivilegeMetadataImpl setAdmin(boolean admin);

}
