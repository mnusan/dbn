package com.dci.intellij.dbn.object.metadata;

import com.dci.intellij.dbn.object.DBType;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBViewMetadata<O extends DBViewMetadata> extends DBObjectMetadata<O> {

    boolean isSystemView();

    O setSystemView(boolean systemView);

    DBType getType();

    O setType(DBType type);

}
