package com.dci.intellij.dbn.object.metadata;

import com.dci.intellij.dbn.language.common.DBLanguage;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBMethodMetadata<O extends DBMethodMetadata> extends DBObjectMetadata<O> {

    int getPosition();

    O setPosition(int position);

    int getOverload();

    O setOverload(int overload);

    DBLanguage getLanguage();

    O setLanguage(DBLanguage language);

    boolean isValid();

    O setValid(boolean valid);

    boolean isDebug();

    O setDebug(boolean debug);

    boolean isDeterministic();

    O setDeterministic(boolean deterministic);

}
