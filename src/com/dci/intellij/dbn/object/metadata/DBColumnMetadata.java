package com.dci.intellij.dbn.object.metadata;

import com.dci.intellij.dbn.data.type.DBDataType;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBColumnMetadata extends DBObjectMetadata<DBColumnMetadata> {

    DBDataType getDataType();

    DBColumnMetadata setDataType(DBDataType dataType);

    int getPosition();

    DBColumnMetadata setPosition(int position);

    boolean isNullable();

    DBColumnMetadata setNullable(boolean nullable);

    boolean isPrimaryKey();

    DBColumnMetadata setPrimaryKey(boolean primaryKey);

    boolean isForeignKey();

    DBColumnMetadata setForeignKey(boolean foreignKey);

    boolean isUniqueKey();

    DBColumnMetadata setUniqueKey(boolean uniqueKey);

    boolean isHidden();

    DBColumnMetadata setHidden(boolean hidden);

}
