package com.dci.intellij.dbn.object.metadata;

import com.dci.intellij.dbn.object.metadata.impl.DBDatabaseLinkMetadataImpl;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBDatabaseLinkMetadata extends DBObjectMetadata<DBDatabaseLinkMetadata> {

    String getUserName();

    DBDatabaseLinkMetadataImpl setUserName(String userName);

    String getHost();

    DBDatabaseLinkMetadataImpl setHost(String host);

}
