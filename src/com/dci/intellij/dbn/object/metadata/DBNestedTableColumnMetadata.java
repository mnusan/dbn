package com.dci.intellij.dbn.object.metadata;

/**
 * @author Manuel Núñez (manuel.nsanchez@gmail.com)
 */
public interface DBNestedTableColumnMetadata extends DBObjectMetadata<DBNestedTableColumnMetadata> {}
