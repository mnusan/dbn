package com.dci.intellij.dbn.object;

public interface DBPackageType<P extends DBTypeProcedure, F extends DBTypeFunction> extends DBType <P,F> {
    public DBPackage getPackage();
}
